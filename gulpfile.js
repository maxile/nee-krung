// Include gulp
var gulp = require('gulp'); 

// Include Our Plugins
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

// Compile Our Sass
gulp.task('sass', function() {
    return gulp.src([
            'resources/assets/scss/*.scss',
            'resources/assets/scss/frontend/*.scss',
        ])
        .pipe(sass())
        .pipe(concat('all.css'))
        .pipe(gulp.dest('public/css/'));
});

gulp.task('sassAdmin', function() {
    return gulp.src([
            'resources/assets/scss/*.scss',
            'resources/assets/scss/backend/*.scss',
        ])
        .pipe(sass())
        .pipe(concat('all-admin.css'))
        .pipe(gulp.dest('public/css/'));
});

// Concatenate & Minify JS
gulp.task('js', function() {
    return gulp.src([
            'resources/assets/js/neekrung.js',
            'resources/assets/js/frontend/**/*.js',
            'resources/assets/js/shared/**/*.js',
        ])
        .pipe(concat('all.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});


gulp.task('jsAdmin', function() {
    return gulp.src([
            'resources/assets/js/neekrungAdmin.js',
            'resources/assets/js/backend/**/*.js',
            'resources/assets/js/shared/**/*.js',
        ])
        .pipe(concat('all-admin.js'))
        .pipe(gulp.dest('public/js'))
        .pipe(rename('all-admin.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'));
});

// Watch Files For Changes
gulp.task('watch', function() {
    gulp.watch('resources/assets/js/**/*.js', ['js',"jsAdmin"]);
    gulp.watch('resources/assets/scss/**/*.scss', ['sass',"sassAdmin"]);
});

// Default Task
gulp.task('default', ['sass',"sassAdmin",'js',"jsAdmin", 'watch']);
