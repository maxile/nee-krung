### Set up
 * install homstead 
 * install npm and bower
 * run `composer update`
 * run `npm install`
 * run `npm install -g gulp`
 * run `npm install -g bower`
 * run `gulp`
 * run `cd public;bower install`

### Set up DB
 * install `mongo`

````
sudo apt-get install -y mongodb-org
````
 * set up PHP's mongo module [mongo.sh](https://github.com/fideloper/Vaprobash/blob/master/scripts/mongodb.sh)

````
 copy this script 
 run (up to line 30)
````

 * run `mongo`
 * set up system user administer
````
use admin
db.createUser(
  {
    user: "siteUserAdmin",
    pwd: "password",
    roles: [ { role: "userAdminAnyDatabase", db: "admin" } ]
  }
)
````
 * set up  user administrator for a single database
````
use neekrung
db.createUser(
  {
    user: "recordsUserAdmin",
    pwd: "password",
    roles: [ { role: "userAdmin", db: "records" } ]
  }
)

````
 * create .env in project directory
```
DB_HOST=localhost
DB_DATABASE=neekrung
DB_USERNAME=**YOURUSERNAME**
DB_PASSWORD=**YOURPASSWORD**

HOST_NAME=http://neekrung.homestead.app
ROOT_PATH=/u/
ADMIN_ROOT_PATH=/admin/
APP_PATH=/

```


### Flexpaper
 * installing the prerequisites

``````
apt-get install pdftk

apt-get install mupdf-tools

mkdir pdf2json

cd pdf2json

wget https://github.com/flexpaper/pdf2json/releases/download/v0.68/pdf2json-0.68.tar.gz

tar -zxvf pdf2json-0.68.tar.gz

./configure

make && make install`
``````

 * installing the zine (optional)

````
download zine http://flexpaper.devaldi.com/download/ 

extract zine to http://localhost 

go to your/zine/directory/php/index.php and setup config path
````


 * if you don't want to download zine, you have to change config file `path.pdf`,`path.swf` in `app/Http/Controllers/config/config.ini.nix.php`

 * copy pdf file to `path.pdf` (Working directory in zine's config)
 * change $doc in FlexpaperController to your pdf (for testing)
 * see the viewer in http://neekrung.homestead.app/flex/viewer


### Backend
 * url : http://neekrung.homestead.app/admin
 * user : admin@neekrung.com
 * password : neekrung

### License
Maxile co,.ltd