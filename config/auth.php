<?php

return [
	'multi' => [
        'member' => [
            'driver' => 'eloquent',
            'model' => 'App\Models\Member',
        ],
        'client' => [
            'driver' => 'eloquent',
            'model' => 'App\Models\User',
        ]
    ],

	'password' => [
		'email' => 'emails.password',
		'table' => 'password_resets',
		'expire' => 60,
	],

];
