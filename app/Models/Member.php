<?php namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;


class Member extends Eloquent implements AuthenticatableContract{
	use Authenticatable;
	//protected $connection = 'mongodb';
	protected $collection = 'members';
}


?>