<?php namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

use App\Models\User;

class Post extends Eloquent{
	//protected $connection = 'mongodb';
	protected $collection = 'posts';

	public function getStoryByAttribute($story_by)
    {
    	if($this->section_name=="wlm"){
    		$user = User::find($story_by);
    		if(isset($user)){
    			return $user->name." ".$user->surname;
    		}
    	}

        return $story_by;
    }
}


?>