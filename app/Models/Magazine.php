<?php namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

class Magazine extends Eloquent{
	//protected $connection = 'mongodb';
	protected $collection = 'magazines';

	protected static function boot() {
        parent::boot();

        static::saved(function($magazine) {
       		if($magazine->published){
       			Magazine::where("_id","!=",$magazine->_id)
       				->update(["published"=>false]);
       		}
        });
    }

}


?>