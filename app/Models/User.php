<?php namespace App\Models;

use Jenssegers\Mongodb\Model as Eloquent;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;


class User extends Eloquent implements AuthenticatableContract, CanResetPasswordContract {
	use Authenticatable, CanResetPassword;
	//protected $connection = 'mongodb';
	protected $collection = 'users';
}


?>