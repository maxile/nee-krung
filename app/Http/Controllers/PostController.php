<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;

use App\Models\Post;
use App\Helper\RandomString;

class PostController extends Controller {

	public function __construct()
	{
		$this->middleware('staff');
	}

	public function recommendedListView(){
		return view("admin.recommended");
	}
	public function recommendedFormView(){
		return view("admin.recommended-form")
				->with("id",NULL);
	}
	public function recommendedEditView($id){
		return view("admin.recommended-form")
				->with("id",$id);
	}


	public function wlmListView(){
		return view("admin.wlm");
	}
	public function wlmFormView(){
		return view("admin.wlm-form")
				->with("id",NULL);
	}
	public function wlmEditView($id){
		return view("admin.wlm-form")
				->with("id",$id);
	}

	public function cityDetoxListView(){
		return view("admin.city-detox");
	}
	public function cityDetoxFormView(){
		return view("admin.city-detox-form")
				->with("id",NULL);
	}
	public function cityDetoxEditView($id){
		return view("admin.city-detox-form")
				->with("id",$id);
	}


	public function newsListView(){
		return view("admin.news");
	}
	public function newsFormView(){
		return view("admin.news-form")
				->with("id",NULL);
	}
	public function newsEditView($id){
		return view("admin.news-form")
				->with("id",$id);
	}
	


	public function getBySection(Request $request,$sectionName){
		$next = 0;
		if($request->has("next")){
			$next = $request->next;
		}

		$post = Post::where("section_name",$sectionName)
					->offset($next)
					->limit(20)
					->get();

		$count = Post::where("section_name",$sectionName)->count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $post,
    		"next"=>$next+20,
    		"count"=> $count
    	]);	
	}

	public function get(Request $request){
		$post = Post::find($request->id);
		if(isset($post)){
			return response()->json([
	    		"message"=>Lang::get("api.get.success"),
	    		"data"=> $post
	    	]);	
		}else{
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}
	}

	public function fetch(Request $request){
		$posts = Post::offset($request->next)
						->limit(20)
						->get();
		$count = Post::count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $posts,
    		"next"=>$request->next+20,
    		"count"=> $count
    	]);	
	}

	private function generateLink($post){
		if($post->section_name!="wlm"){
			$post->link= $post->section_name."/".$post->route_name;	
		}else{
			$post->link= "world-longest-magazine/".$post->route_name;	
		}
	}

	public function store(Request $request){

		$post = new Post;
		
		$validator = Validator::make($request->all(), [
			"section_name"=>"required",
            'category' => 'required',
            'title' => 'required',
            'image' => 'required',
            'short_detail' => 'required',
            'contents' => 'array',
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

        
        $post->section_name = $request->section_name;
        $post->category = $request->category;
        $post->title = $request->title;
        $post->image = $request->image;
        $post->short_detail = $request->short_detail;
        $post->contents = $request->contents;

        $this->notRequiredAttr($post,$request,"category_name",false);
        $this->notRequiredAttr($post,$request,"route_name",true,RandomString::generate(5));
        $this->notRequiredAttr($post,$request,"connect_payment",false);
        $this->notRequiredAttr($post,$request,"story_by");
        $this->notRequiredAttr($post,$request,"image_by");
        $this->notRequiredAttr($post,$request,"published",true,false);
        $this->generateLink($post);

		if(!$post->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function update(Request $request){
		$post = Post::find($request->id);
		if(!isset($post)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		$validator = Validator::make($request->all(), [
			"section_name"=>"required",
            'category' => 'required',
            'title' => 'required',
            'image' => 'required',
            'short_detail' => 'required',
            'contents' => 'array',
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

        $post->category = $request->category;
        $post->title = $request->title;
        $post->image = $request->image;
        $post->short_detail = $request->short_detail;
        $post->contents = $request->contents;

        $this->notRequiredAttr($post,$request,"category_name",false);
        $this->notRequiredAttr($post,$request,"route_name",true,$post->route_name);
        $this->notRequiredAttr($post,$request,"connect_payment",false);
        $this->notRequiredAttr($post,$request,"story_by");
        $this->notRequiredAttr($post,$request,"image_by");
        $this->notRequiredAttr($post,$request,"published",true,false);
        $this->generateLink($post);
		
		if(!$post->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function delete(Request $request){
		$post = Post::find($request->id);
		if(!isset($post)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		if(!$post->delete()){
			return response()->json([
	    		"message"=>Lang::get("api.delete.fail")
	    	],500);		
		}

		return response()->json([
    		"message"=>Lang::get("api.get.success")
    	]);	

	}

}
