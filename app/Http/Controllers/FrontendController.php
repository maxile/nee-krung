<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;

use App\Models\Magazine;
use App\Models\Post;

class FrontendController extends Controller {

	public function __construct()
	{
		
	}


	public function getIndex(){

		$magazine = Magazine::where("published",true)->first();
		$magazines = Magazine::orderBy('vol',"DESC")->limit(6)->get();

		$sections = [
			// "recommended" =>[
			// 	"numberOf"=>3
			// ],
			"city-detox" =>[
				"numberOf"=>3
			],
			"wlm" =>[
				"numberOf"=>6
			],
			"news" =>[
				"numberOf"=>1
			],
		];


		foreach ($sections as $sectionName => $sectionInfo) {
			$posts = Post::where("published",true)
						->where('section_name',$sectionName)
						->orderBy("created_at","DESC")
						->limit($sectionInfo["numberOf"])
						->select([
							"_id",
							"section_name",
							"category",
							"title",
							"image",
							"short_detail",
							"story_by",
							"image_by",
							"link"
						])
						->get();

			$sections[$sectionName]["posts"] = $posts;
		}

		$sections["recommended"] = [
			"numberOf"=>3,
			"posts" => $this->getRecommendedEachCategory(false)
		];

		return response()->json([
			"magazine"=>$magazine,
			"books"=>$magazines,
			"sections"=>$sections
		]);
	}

	public function getRecommendedEachCategory($isRequest = true){
		$posts = [];
		$categories = ["eat","sleep","travel","fun"];
		foreach ($categories as $category) {
			$p = Post::where("published",true)
				->where('section_name',"recommended")
				->where("category_name",$category)
				->orderBy("created_at","DESC")
				->select([
					"_id",
					"section_name",
					"category",
					"category_name",
					"title",
					"image",
					"short_detail",
					"story_by",
					"image_by",
					"link"
				])
				->first();	
			if(isset($p)){
				array_push($posts, $p);	
			}
			
		}

		if($isRequest){
			return response()->json([
	    		"message"=>Lang::get("api.get.success"),
	    		"data"=> $posts
	    	]);	
		}
		
		return $posts;
	}

	public function getPostBySection(Request $request,$sectionName){

		$next = 0;
		if($request->has("next")){
			$next = $request->next;
		}

		$post = Post::where("section_name",$sectionName)
					->where("published",true)
					->orderBy("created_at","DESC")
					->offset($next)
					->select([
						"category",
						"title",
						"image",
						"short_detail",
						"story_by",
						"image_by",
						"link",
						"route_name",
						"created_at"
					])
					->limit(20)
					->get();

						
		$count = Post::where("section_name",$sectionName)
						->where("published",true)
						->count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $post,
    		"next"=>$next+20,
    		"count"=> $count
    	]);	
	}

	public function getRecommendedByCategory(Request $request,$category){

		$next = 0;
		if($request->has("next")){
			$next = $request->next;
		}

		$post = Post::where("published",true)
				->where('section_name',"recommended")
				->where("category_name",$category)
				->orderBy("created_at","DESC")
				->offset($next)
				->select([
					"category",
					"title",
					"image",
					"short_detail",
					"story_by",
					"image_by",
					"link",
					"route_name"
				])
				->limit(20)
				->get();
						
		$count = Post::where('section_name',"recommended")
						->where("published",true)
						->where("category_name",$category)
						->count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $post,
    		"next"=>$next+20,
    		"count"=> $count
    	]);	
	}

	public function getPostByID($postID){

		$post = Post::where("route_name",$postID)
					->where("published",true)
					->first();

		if(!isset($post)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail"),
	    		"data"=> $post,
	    	],500);
		}
		
		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $post,
    	]);	
	}


	public function getRelatedPost($postID){
		$posts = Post::where("published",true)
					->where("section_name","wlm")
					->limit(3)
					->get();

		return response()->json([
			"message"=>Lang::get("api.get.success"),
    		"data"=> $posts
		]);
	}


	public function getPostByUser(Request $request,$userID){

		$next = 0;
		if($request->has("next")){
			$next = $request->next;
		}

		$post = Post::where("section_name","wlm")
					->where("published",true)
					->orderBy("created_at","DESC")
					->where("created_by",$userID)
					->offset($next)
					->select([
						"category",
						"title",
						"image",
						"short_detail",
						"story_by",
						"image_by",
						"link"
					])
					->limit(20)
					->get();

						
		$count = Post::where("section_name","wlm")
					->where("published",true)
					->orderBy("created_at","DESC")
					->where("created_by",$userID)
					->count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $post,
    		"next"=>$next+20,
    		"count"=> $count
    	]);	
	}
	
}
