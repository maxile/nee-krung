<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;
use App\Helper\RandomString;

class UploadController extends Controller {

	public function __construct()
	{
		$this->middleware('staff.or.user');
	}

	public function image(Request $request){

		$validator = Validator::make($request->all(), [
            'neekrungImage' => 'required|image'
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

		$image = $request->file("neekrungImage");

		$originalName = $image->getClientOriginalName();
		
		$imageName = (RandomString::generate(5)).".".pathinfo($originalName, PATHINFO_EXTENSION);
		$storePath = 'images/';

	    $image->move(public_path()."/".$storePath, $imageName);
	    return response()->json([ 
	    	"message"=>Lang::get("api.store.success"),
	    	"data"=> $storePath.$imageName
	    ]);
	}

	public function pdf(Request $request){

		$validator = Validator::make($request->all(), [
            'neekrungPDF' => 'required'
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

		$file = $request->file("neekrungPDF");

		$originalName = $file->getClientOriginalName();
		
		$randomName = RandomString::generate(5);
		$fullName = $randomName.".".pathinfo($originalName, PATHINFO_EXTENSION);
		$storePath = base_path()."/resources/books/";

	    $file->move($storePath, $fullName);
	    return response()->json([ 
	    	"message"=>Lang::get("api.store.success"),
	    	"data"=> $randomName
	    ]);
	}

	

	
}
