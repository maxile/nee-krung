<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;

abstract class Controller extends BaseController {

	use DispatchesCommands, ValidatesRequests;
	
	protected function notRequiredAttr($object,$request,$attribute,$isDefault = true,$default=null){

		if($isDefault){
			$object->$attribute = $default;
		}
		if($request->has($attribute)){
        	$object->$attribute = $request->$attribute;	
        }
	}
}
