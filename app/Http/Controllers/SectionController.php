<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;

use App\Models\Section;

class SectionController extends Controller {

	public function __construct()
	{
		$this->middleware('staff');
	}

	public function get(Request $request){
		$section = Section::find($request->id);
		if(isset($m)){
			return response()->json([
	    		"message"=>Lang::get("api.get.success"),
	    		"data"=> $section
	    	]);	
		}else{
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}
	}

	public function fetch(Request $request){
		$sections = Section::offset($request->next)
						->limit(20)
						->get();
		$count = Section::count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $sections,
    		"next"=>$request->next+20,
    		"count"=> $count
    	]);	
	}

	public function store(){
		$section = new Section;
		
		//// do smt
		
		if(!$section->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function update(Request $request){
		$section = Section::find($request->id);
		if(!isset($section)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		//// do smt
		$section->save();
	}

	public function delete(Request $request){
		$section = Section::find($request->id);
		if(!isset($section)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		if(!$section->delete()){
			return response()->json([
	    		"message"=>Lang::get("api.delete.fail")
	    	],500);		
		}

		return response()->json([
    		"message"=>Lang::get("api.get.success")
    	]);	

	}
}
