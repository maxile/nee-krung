<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;
use Auth;
use App\Models\Member;

class MemberController extends Controller {

	public function __construct()
	{
		$this->middleware('staff',["except"=>["loginView","login"]]);
		$this->middleware('not.staff',["only"=>["loginView","login"]]);
	}

	public function memberListView(){
		return view("admin.member");
	}
	public function memberFormView(){
		return view("admin.member-form")
				->with("id",NULL);
	}
	public function memberEditView($id){
		return view("admin.member-form")
				->with("id",$id);
	}

	public function loginView(){
		return view('admin.login');
	}

	public function login(Request $request){

		if (Auth::member()->attempt(['email' => $request->email, 'password' => $request->password]))
        {
        	$user = Auth::member()->user();
            return response()->json([
        		"message"=>Lang::get("user.login.success"),
        		"user"=>$user
        	]);
        }

        return response()->json(["message"=>Lang::get("user.login.fail")],500);
	}

	public function logout(){
		Auth::member()->logout();
		return response()->json(["message"=>Lang::get("user.logout")]);
	}


	public function get(Request $request){
		$member = Member::find($request->id);
		if(isset($member)){
			return response()->json([
	    		"message"=>Lang::get("api.get.success"),
	    		"data"=> $member
	    	]);	
		}else{
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}
	}

	public function fetch(Request $request){
		$members = Member::offset($request->next)
						->limit(20)
						->get();

		$count = Member::count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $members,
    		"next"=>$request->next+20,
    		"count"=> $count
    	]);	
	}

	public function store(Request $request){

		
		$validator = Validator::make($request->all(), [
			'email' => 'required|email|unique:members',
            'password' => 'required|min:5|max:20',
            'repassword' => 'required|same:password',
            "image_url"=>"required",
            "name_th"=>"required",
            "surname_th"=>"required",
            "name_eng"=>"required",
            "surname_eng"=>"required"
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

		$member = new Member;        
		$member->image_url = $request->image_url;
		$member->email = $request->email;
		$member->password = Hash::make($request->password);

		$member->name_th = $request->name_th;
		$member->surname_th = $request->surname_th;
		$member->name_eng = $request->name_eng;
		$member->surname_eng = $request->surname_eng;

		if($request->has("education_field"));
			$member->education_field = $request->education_field;

		if($request->has("university"));
			$member->university = $request->university;

		if($request->has("university_address"));
			$member->university_address = $request->university_address;

		$member->is_admin = false;
		$member->layout = rand(1, 3);

		if(!$member->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function update(Request $request){
		$member = Member::find($request->id);
		if(!isset($member)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		$validator = Validator::make($request->all(), [
            'repassword' => 'same:password',
            "image_url"=>"required",
            "name_th"=>"required",
            "surname_th"=>"required",
            "name_eng"=>"required",
            "surname_eng"=>"required"
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }


        $member->image_url = $request->image_url;
		if($request->has("password")){
			$member->password = Hash::make($request->password);	
		}

		$member->name_th = $request->name_th;
		$member->surname_th = $request->surname_th;
		$member->name_eng = $request->name_eng;
		$member->surname_eng = $request->surname_eng;

		if($request->has("education_field"));
			$member->education_field = $request->education_field;

		if($request->has("university"));
			$member->university = $request->university;

		if($request->has("university_address"));
			$member->university_address = $request->university_address;

		
		if(!$member->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function delete(Request $request){
		$member = Member::find($request->id);
		if(!isset($member)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		if($member->is_admin){
			return response()->json([
	    		"message"=>Lang::get("api.delete.fail")
	    	],500);		
		}

		if(!$member->delete()){
			return response()->json([
	    		"message"=>Lang::get("api.delete.fail")
	    	],500);		
		}

		return response()->json([
    		"message"=>Lang::get("api.get.success")
    	]);	

	}

}
