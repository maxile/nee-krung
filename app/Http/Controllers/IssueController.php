<?php namespace App\Http\Controllers;

use App\Models\Magazine;
use App\Models\Post;

use App\Helper\RandomString;

class IssueController extends Controller {
	public function update(){
		// $this->updateRecommendedPost();
		$this->updateWLMPost();
		return response()->json([
			"message"=>"success"
		]);
	}
	
	private function updateMagazine(){
		$magazines = Magazine::all();
		foreach ($magazines as $index => $magazine) {
			$magazine->magazine_file = "demo".($index+1);
			$magazine->image = [
				"image_url"=> $magazine->banners[0]["image_url"],
				"caption"=> $magazine->banners[0]["caption"],
				"longitude"=>0,
				"latitude"=>0
			];

			$magazine->cover = $magazine->image_url;
			
			$magazine->unset('banners');
			$magazine->unset('image_url');

			$magazine->link="";

			$magazine->save();
		}
	}
	

	private function updatePost(){
		$posts = Post::all();
		foreach ($posts as $index => $post) {
			$post->image = [
				"image_url"=> $post->image_url,
				"longitude"=>0,
				"latitude"=>0
			];
			$post->unset('image_url');
			$post->route_name = RandomString::generate(5);

			if($post->section_name!="wlm"){
				$post->link= $post->section_name."/".$post->route_name;	
			}else{
				$post->link= "world-longest-magazine/".$post->route_name;	
			}
			
			$post->save();
		}
	}

	private function updateRecommendedPost(){
		$posts = Post::where("section_name","recommended")->get();
		foreach ($posts as $index => $post) {
			$post->link= $post->section_name."/".$post->category_name."/".$post->route_name;	
			$post->save();

			for($i=0;$i<10;$i++){
				$newPost = new Post();

				$newPost->section_name = $post->section_name;
		        $newPost->category = $post->category;
		        $newPost->title = $post->title;
		        $newPost->image = $post->image;
		        $newPost->short_detail = $post->short_detail;
		        $newPost->contents = [];
		        $newPost->category_name = $post->category_name;
		        $newPost->route_name = RandomString::generate(5);
		        $newPost->story_by = null;
		        $newPost->image_by = null;
		        $newPost->published = true;

		        $newPost->link= $newPost->section_name."/".$newPost->category_name."/".$newPost->route_name;
		        $newPost->save();
			}
			
		}
	}

	private function updateWLMPost(){
		$template = Post::where("_id","56af27aebffebc9a2c8b4568")->first();

		$posts = Post::where("section_name","wlm")->get();
		foreach ($posts as $index => $post) {
			$post->contents = $template->contents;
			$post->save();
		}
	}
}
