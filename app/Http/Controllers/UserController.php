<?php namespace App\Http\Controllers;

use App\Models\User;
use Validator;
use Illuminate\Http\Request;
use Lang;
use Hash;
use Auth;

class UserController extends Controller {

	public function __construct()
	{
		// $this->middleware('auth');
		$this->middleware('auth', ['only' => ['logout']]);
		$this->middleware('guest', ['only' => ['signup','login','facebook']]);
	}

	public function signup(Request $request){
		$validator = Validator::make($request->all(), [
            'email' => 'required|email|unique:users',
            'password' => 'required|min:5|max:20',
            'repassword' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

		$user = new User;
		$user->email = $request->email;
		$user->password = Hash::make($request->password);
		$user->name = $request->name;
		$user->surname = $request->surname;
		$user->image_url = "image/user.jpg";
		$user->promotion = "free";
		$user->fb_access_token = NULL;
		$user->fb_id = NULL;
		
		
		if($user->save()){
			Auth::client()->attempt(['email' => $user->email, 'password' => $request->password]);

			return response()->json([
				"message"=>Lang::get("user.create.success",["email"=>$user->email]),
				"data"=>$user
			]);
		}

		return response()->json(["message"=>Lang::get("user.create.fail")],500);

	}

	public function login(Request $request){

		if (Auth::client()->attempt(['email' => $request->email, 'password' => $request->password]))
        {
        	$user = Auth::client()->user();
            return response()->json([
        		"message"=>Lang::get("user.login.success"),
        		"data"=>$user
        	]);
        }

        return response()->json(["message"=>Lang::get("user.login.fail")],500);
	}

	public function logout(){
		Auth::client()->logout();
		return response()->json(["message"=>Lang::get("user.logout")]);
	}

	public function getUser($id=null){

		if(is_null($id)){
			if(Auth::client()->check()){
				$user = Auth::client()->user();
		        return response()->json([
		    		"message"=>Lang::get("user.get.success"),
		    		"data"=>$user
		    	]);	
			}else{
		        return response()->json([
		    		"message"=>Lang::get("user.get.fail")
		    	],500);	
			}	
		}else{
			$user = User::find($id);
			if(isset($user)){
				return response()->json([
		    		"message"=>Lang::get("user.get.success"),
		    		"data"=>$user
		    	]);	
			}else{
				return response()->json([
		    		"message"=>Lang::get("user.get.fail")
		    	],500);	
			}
		}
		
		
	}

	public function facebook(Request $request){

		$fbToken = $request->token;
		$userInfo = $request->userInfo;
		$user = User::where("fb_id",$fbToken["userID"])->first();
		$password = $userInfo["id"].env("APP_KEY","m@xiL3");
		if(!isset($user)){
			$user = new User;
			$user->email = $userInfo["email"];
			$user->password = Hash::make($password);
			$user->name = $userInfo["first_name"];
			$user->surname = $userInfo["last_name"];
			$user->image_url = "image/user.jpg";
			$user->promotion = "free";
			$user->fb_access_token = $fbToken["accessToken"];
			$user->fb_id = $userInfo["id"];
			if(!$user->save()){
				return response()->json(["message"=>Lang::get("user.create.fail")],500);
			}
		}
		
		if (Auth::client()->attempt(['email' => $user->email, 'password' => $password]))
        {
        	$user = Auth::client()->user();
            return response()->json([
        		"message"=>Lang::get("user.login.success"),
        		"data"=>$user
        	]);
        }

        return response()->json(["message"=>Lang::get("user.login.fail")],500);

	}
}
