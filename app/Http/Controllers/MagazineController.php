<?php namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Lang;

use App\Models\Magazine;

class MagazineController extends Controller {

	public function __construct()
	{
		$this->middleware('staff');
	}
	public function listView(){
		return view("admin.magazine");
	}
	public function formView(){
		return view("admin.magazine-form")
				->with("id",NULL);
	}

	public function editView($id){
		return view("admin.magazine-form")
				->with("id",$id);
	}

	public function get(Request $request,$id){

		$magazine = Magazine::find($id);
		if(isset($magazine)){
			return response()->json([
	    		"message"=>Lang::get("api.get.success"),
	    		"data"=> $magazine
	    	]);	
		}else{
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}
	}

	public function fetch(Request $request){
		$next = 0;
		if($request->has("next")){
			$next = $request->next;
		}
		
		$magazines = Magazine::offset($next)
						->orderBy('vol',"DESC")
						->limit(20)
						->get();
		$count = Magazine::count();

		return response()->json([
    		"message"=>Lang::get("api.get.success"),
    		"data"=> $magazines,
    		"next"=>$next+20,
    		"count"=> $count
    	]);	
	}

	public function store(Request $request){
		$magazine = new Magazine;
		$validator = Validator::make($request->all(), [
            'vol' => 'required',
            'month' => 'required',
            'year' => 'required',
            'image' => 'required',
            'cover' => 'required',
            "magazine_file"=>"required"
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }
        
		$magazine->vol = $request->vol;
		$magazine->month = $request->month;
		$magazine->year = $request->year;
		$magazine->image = $request->image;
		$magazine->magazine_file = $request->magazine_file;
		$magazine->published = false;
		$magazine->cover = $request->cover;
		$magazine->link="";
		
		if($request->has("published")){
			$magazine->published = $request->published;	
		}

		if(!$magazine->save()){
			return response()->json([
	    		"message"=>Lang::get("api.store.fail")
	    	],500);	
		}

		return response()->json([
    		"message"=>Lang::get("api.store.success")
    	]);	
	}

	public function update(Request $request){
		$magazine = Magazine::find($request->id);
		if(!isset($magazine)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		$validator = Validator::make($request->all(), [
            'vol' => 'required',
            'month' => 'required',
            'year' => 'required',
            'image' => 'required',
            'cover' => 'required',
            "magazine_file"=>"required"
        ]);

		if ($validator->fails()) {
            $messages = $validator->messages();
        	return response()->json([ "message" => $messages],500);
        }

		$magazine->vol = $request->vol;
		$magazine->month = $request->month;
		$magazine->year = $request->year;
		$magazine->image = $request->image;
		$magazine->magazine_file = $request->magazine_file;
		$magazine->cover = $request->cover;

		$magazine->published = false;
		if($request->has("published")){
			$magazine->published = $request->published;	
		}
		$magazine->save();

		return response()->json([
    		"message"=>Lang::get("api.store.success"),
    		"data"=>$magazine
    	]);	
	}

	public function delete(Request $request){
		$magazine = Magazine::find($request->id);
		if(!isset($magazine)){
			return response()->json([
	    		"message"=>Lang::get("api.get.fail")
	    	],500);	
		}

		if(!$magazine->delete()){
			return response()->json([
	    		"message"=>Lang::get("api.delete.fail")
	    	],500);		
		}

		return response()->json([
    		"message"=>Lang::get("api.get.success")
    	]);	

	}

	

	
}
