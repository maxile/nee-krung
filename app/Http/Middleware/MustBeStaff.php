<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;
class MustBeStaff {

	public function handle($request, Closure $next)
	{
		if(!Auth::member()->check() ){
			return redirect()->to(env("ADMIN_ROOT_PATH"));
		}

		return $next($request);	
		
	}

}
