<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Auth;

class Authenticate {

	
	public function handle($request, Closure $next)
	{


		if (Auth::client()->guest())
		{

			if ($request->ajax())
			{
				return response('Unauthorized.', 401);
			}
			else
			{
				return redirect()->to(env("ROOT_PATH"));
			}
		}

		return $next($request);
	}

}
