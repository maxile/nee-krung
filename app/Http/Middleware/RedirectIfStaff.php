<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Auth;

class RedirectIfStaff {

	
	public function handle($request, Closure $next)
	{
		if (Auth::member()->check())
		{
			if ($request->ajax())
			{
				return response()->json([]);
			}
			else
			{
				return redirect()->to('/admin/magazine');
			}
		}
		return $next($request);
	}

}
