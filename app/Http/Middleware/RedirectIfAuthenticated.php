<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
use Auth;

class RedirectIfAuthenticated {

	
	public function handle($request, Closure $next)
	{
		if (Auth::client()->check())
		{
			if ($request->ajax())
			{
				return response()->json([]);
			}
			else
			{
				return redirect()->to('/u/member');
			}
		}

		return $next($request);
	}

}
