<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['prefix' => 'admin'], function(){
	// Route::get('/u/p/d/a/t/e', [ 'uses'=>'IssueController@update']);
	Route::get('/', [ "as"=>'admin.login','uses'=>'MemberController@loginView']);
	
	Route::group(['prefix' => 'magazine'], function(){
		Route::get('/', [ "as"=>'magazine.list','uses'=>'MagazineController@listView']);
		Route::get('/create', [ "as"=>'magazine.form','uses'=>'MagazineController@formView']);
		Route::get('/{id}/edit', [ "as"=>'magazine.edit','uses'=>'MagazineController@editView']);	
	});

	Route::group(['prefix' => 'recommended'], function(){
		Route::get('/', [ "as"=>'recommended.list','uses'=>'PostController@recommendedListView']);
		Route::get('/create', [ "as"=>'recommended.form','uses'=>'PostController@recommendedFormView']);
		Route::get('/{id}/edit', [ "as"=>'recommended.edit','uses'=>'PostController@recommendedEditView']);	
	});

	Route::group(['prefix' => 'wlm'], function(){
		Route::get('/', [ "as"=>'wlm.list','uses'=>'PostController@wlmListView']);
		Route::get('/create', [ "as"=>'wlm.form','uses'=>'PostController@wlmFormView']);
		Route::get('/{id}/edit', [ "as"=>'wlm.edit','uses'=>'PostController@wlmEditView']);	
	});

	Route::group(['prefix' => 'citydetox'], function(){
		Route::get('/', [ "as"=>'citydetox.list','uses'=>'PostController@cityDetoxListView']);
		Route::get('/create', [ "as"=>'citydetox.form','uses'=>'PostController@cityDetoxFormView']);
		Route::get('/{id}/edit', [ "as"=>'citydetox.edit','uses'=>'PostController@cityDetoxEditView']);	
	});

	Route::group(['prefix' => 'news'], function(){
		Route::get('/', [ "as"=>'news.list','uses'=>'PostController@newsListView']);
		Route::get('/create', [ "as"=>'news.form','uses'=>'PostController@newsFormView']);
		Route::get('/{id}/edit', [ "as"=>'news.edit','uses'=>'PostController@newsEditView']);	
	});

	Route::group(['prefix' => 'member'], function(){
		Route::get('/', [ "as"=>'member.list','uses'=>'MemberController@memberListView']);
		Route::get('/create', [ "as"=>'member.form','uses'=>'MemberController@memberFormView']);
		Route::get('/{id}/edit', [ "as"=>'member.edit','uses'=>'MemberController@memberEditView']);	
	});
	
});

Route::group(['prefix' => 'api'], function(){
	Route::group(['prefix' => 'user'], function(){
		Route::post('/create', ['uses'=>'UserController@signup']);
		Route::post('/create/facebook', ['uses'=>'UserController@facebook']);
		
		Route::post('/login', ['uses'=>'UserController@login']);
		Route::get('/logout', ['uses'=>'UserController@logout']);

		Route::get('/{id?}', [ 'uses'=>'UserController@getUser']);
	});

	Route::group(['prefix' => 'magazine'], function(){
		Route::get('/', [ 'uses'=>'MagazineController@fetch']);
		Route::get('/{id}', ['uses'=>'MagazineController@get']);
		Route::post('/', ['uses'=>'MagazineController@store']);
		
		Route::put('/{id}', ['uses'=>'MagazineController@update']);
		Route::delete('/{id}', ['uses'=>'MagazineController@delete']);
	});

	Route::group(['prefix' => 'post'], function(){
		Route::get('/', [ 'uses'=>'PostController@fetch']);
		Route::get('/section/{sectionName}', ['uses'=>'PostController@getBySection']);
		Route::get('/{id}', ['uses'=>'PostController@get']);

		Route::post('/', ['uses'=>'PostController@store']);
		
		Route::put('/{id}', ['uses'=>'PostController@update']);
		Route::delete('/{id}', ['uses'=>'PostController@delete']);
	});

	

	Route::group(['prefix' => 'member'], function(){
		Route::post('/login', ['uses'=>'MemberController@login']);
		Route::get('/logout', ['uses'=>'MemberController@logout']);

		Route::get('/', [ 'uses'=>'MemberController@fetch']);
		Route::get('/{id}', ['uses'=>'MemberController@get']);
		Route::post('/', ['uses'=>'MemberController@store']);
		
		Route::put('/{id}', ['uses'=>'MemberController@update']);
		Route::delete('/{id}', ['uses'=>'MemberController@delete']);
	});

	Route::group(['prefix' => 'upload'], function(){
		Route::post('/image', ['uses'=>'UploadController@image']);
		Route::post('/pdf', ['uses'=>'UploadController@pdf']);
	});

	Route::group(['prefix' => 'get'], function(){
		Route::get('/index', ['uses'=>'FrontendController@getIndex']);
		Route::get('/post/section/recommended/category', ['uses'=>'FrontendController@getRecommendedEachCategory']);
		Route::get('/post/section/recommended/category/{category}', ['uses'=>'FrontendController@getRecommendedByCategory']);
		Route::get('/post/section/{section}', ['uses'=>'FrontendController@getPostBySection']);
		Route::get('/post/user/{userID}', ['uses'=>'FrontendController@getPostByUser']);
		Route::get('/post/related/{postID}', ['uses'=>'FrontendController@getRelatedPost']);
		Route::get('/post/{postID}', ['uses'=>'FrontendController@getPostByID']);
		
	});

	
});

Route::group(['prefix' => 'flex'], function(){
	Route::get('/service', ['uses'=>'FlexpaperController@service']);
	Route::get('/viewer', ['uses'=>'FlexpaperController@viewer']);
	
});

Route::get('/', function(){
	return redirect()->to("/u");
});

Route::group(['prefix' => 'u'], function(){
	Route::any('{all?}', function(){
		return view('front.app');
	})
	->where('all', '.+');
});

