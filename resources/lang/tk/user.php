<?php

return [
    'create' => [
    	"success"=>"Registration complete, :email.",
    	"fail"=>"Cannot Create This User."
    ],
    'login' => [
    	"success"=>"Login complete",
    	"fail"=>"Cannot Login,Please check your type and try again."
    ],
    "get" => [
        "success"=>"Successful to get your info.",
        "fail"=>"Don't have this user."
    ],
    "logout"=> "logout complete"
];

?>