<?php

return [
    'get' => [
    	"success"=>"Get your data complete.",
    	"fail"=>"Sorry, not found your data."
    ],
    'store' => [
        "success"=>"Your data was stored.",
        "fail"=>"There are few mistakes, Please try again."
    ],
    'update' => [
        "success"=>"Your data was updated.",
        "fail"=>"There are few mistakes, Please try again."
    ], 
    'delete' => [
        "success"=>"Your data was deleted.",
        "fail"=>"There are few mistakes, Please try again."
    ]

];

?>