angular.module('neekrungApp')
.factory('postTranform', ["ngImage","URL_CONSTANT","ROUTE_NAME",function(ngImage,URL_CONSTANT,ROUTE_NAME){
	return {
		addCredit: function(post){
			post.credit = [];
			if(post.story_by){
				post.credit.push("Text / "+post.story_by);
			}

			if(post.image_by){
				post.credit.push("Photo / "+post.image_by);
			}
		},
		addLink:function(post){
			
			// if(post.section_name=="wlm"){
			// 	post.link = URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM_POST+post._id;
			// }else{
				post.link = URL_CONSTANT.ROOT_PATH+post.link;	
			// }
			
		},
		
		toImageBg: function(post){
			post.bgStyle= { 'background-image': "url("+ngImage.imageURL(post.image.image_url)+")"};
		},

		imageURL: function(post){
			post.imageURL= ngImage.imageURL(post.image.image_url);
		}
 	}


}]);