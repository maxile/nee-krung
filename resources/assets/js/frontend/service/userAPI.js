angular.module('neekrungApp')
.factory('userAPI', ["API","$http",function(API,$http){
	return {
		login:function(data){
			return $http({
			    method: 'POST',
			    url: API.USER_LOGIN,
			    data:data
			})
		},
		logout:function(){
			return $http({
			    method: 'GET',
			    url: API.USER_LOGOUT
			})
		},
		signup:function(data){
			return $http({
			    method: 'POST',
			    url: API.USER_SIGNUP,
			    data:data
			});
		},
		update:function(){
			
		},
		getUser: function(uid){
			if(!uid){
				uid="";
			}
			return $http({
			    method: 'GET',
			    url: API.USER_GET+"/"+uid
			})
		},
 	}


}]);