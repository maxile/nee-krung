angular.module('neekrungApp')
.config(["FacebookProvider",function(FacebookProvider) {
	FacebookProvider.init('801832756609909');
}])
.factory('facebookAPI', ["Facebook","userAPI","API","$q","$http",function(Facebook,userAPI,API,$q,$http){
	var vm = this;
	return {
		login : function(){
			return $q(function(resolve, reject) {
			    Facebook.login(function(resp){
					if(resp.status=="connected"){
						Facebook.api('/me/?fields=first_name,last_name,email,gender', function(user) {
					        $http({
							    method: 'POST',
							    url: API.USER_FB,
							    data:{
							    	userInfo:user,
							    	token: resp.authResponse
							    }
							}).then(function(resp){
								resolve(resp);
							},function(err){
								reject(err);
							});
						});
					}else{
						reject(resp);	
					}
				},{scope: ['email',"public_profile"]});
			});
		},
		isReady : function(){
			return Facebook.isReady();
		}	
 	}


}]);