angular.module('neekrungApp')
.constant('API',{
	USER_LOGIN:"/api/user/login",
	USER_LOGOUT:"/api/user/logout",
	USER_SIGNUP:"/api/user/create",	
	USER_GET:"/api/user",
	USER_FB:"/api/user/create/facebook",
	GET_INDEX:"/api/get/index",

	GET_POST_BY_ID:"/api/get/post/",
	GET_POST_BY_USER:"/api/get/post/user/",
	GET_RELATED_POST:"/api/get/post/related/",
	GET_POST_BY_SECTION:"/api/get/post/section/",
	GET_RECOMMENDED_EACH_CATEGORY: "/api/get/post/section/recommended/category",
	GET_RECOMMENDED_BY_CATEGORY:"/api/get/post/section/recommended/category/",
	IMAGE_UPLOAD: "api/upload/image/",

})