angular.module('neekrungApp')
.factory('postAPI', ["API","$http",function(API,$http){
	return {
		getIndex: function(){
			return $http({
			    method: 'GET',
			    url: API.GET_INDEX
			})
		},
		getPostBySection: function(section){
			return $http({
			    method: 'GET',
			    url: API.GET_POST_BY_SECTION+section
			})	
		},
		getPostByID:function(postID){
			return $http({
			    method: 'GET',
			    url: API.GET_POST_BY_ID+postID
			})		
		},
		getRelatedPost:function(postID){
			return $http({
			    method: 'GET',
			    url: API.GET_RELATED_POST+postID
			})			
		},
		getPostByUsername:function(userID){
			return  $http({
			    method: 'GET',
			    url: API.GET_POST_BY_USER+userID
			})		
		},
		getRecommendedEachCategory:function(){
			return  $http({
			    method: 'GET',
			    url: API.GET_RECOMMENDED_EACH_CATEGORY
			})			
		},
		getRecommendedByCategory:function(category){
			return  $http({
			    method: 'GET',
			    url: API.GET_RECOMMENDED_BY_CATEGORY+category
			})			
		}
 	}


}]);