angular.module('neekrungApp')
.controller('wlmPostController', ["Global","postAPI","postTranform","ROUTE_NAME","$routeParams","URL_CONSTANT","Notice","$scope","$location",function(Global,postAPI,postTranform,ROUTE_NAME,$routeParams,URL_CONSTANT,Notice,$scope,$location) {
	var vm = this;

	Global.selectedMenu="wlm";
	Global.initialComplete=false;
	vm.url = URL_CONSTANT.HOST_NAME+URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM_POST+$routeParams.postName;

	vm.post = {};
	vm.relatedPost = {
		"title":"Releated Article",
		"template": "preview/preview-3-col-extra.html",
		"posts":[]
	}
	
	postAPI.getPostByID($routeParams.postName)
	.success(function(resp){
		Global.initialComplete = true;
		vm.post = resp.data;
		vm.post.story_by = "ฉันเอง";
		postTranform.addCredit(vm.post);

	}).error(function(resp){
		$scope.message = resp.message;
		Notice.alert($scope)
		.result.finally(function(){
			$location.path(URL_CONSTANT.ROOT_PATH);
		});
	});

	postAPI.getRelatedPost($routeParams.postName)
	.success(function(resp){
		vm.relatedPost.posts = resp.data;
		angular.forEach(resp.data,function(post){
			postTranform.addCredit(post);
			postTranform.toImageBg(post);
		});
		
	});

}]);