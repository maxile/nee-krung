angular.module('neekrungApp')
.controller('recommendedCategoryController', ["URL_CONSTANT","Global","postAPI","postTranform","$routeParams",function(URL_CONSTANT,Global,postAPI,postTranform,$routeParams) {
	var vm = this;
	Global.selectedMenu="recommended";
	Global.initialComplete=false;

	vm.subTitle="น่าน่าน่าน่า";
	vm.posts=[];
	postAPI.getRecommendedByCategory($routeParams.category)
	.success(function(resp){
		Global.initialComplete = true;

		angular.forEach(resp.data,function(post){
			post.story_by = "ฉันเขียนเอง";
			postTranform.addCredit(post);
			postTranform.imageURL(post);
			postTranform.addLink(post);
		});

		vm.posts = resp.data;
	});
	


}]);