angular.module('neekrungApp')
.controller('recommendedController', ["URL_CONSTANT","Global","postAPI","postTranform","ROUTE_NAME",function(URL_CONSTANT,Global,postAPI,postTranform,ROUTE_NAME) {
	var vm = this;
	Global.selectedMenu="recommended";
	Global.initialComplete=false;

	vm.title="Recommended";
	vm.subTitle="น่าน่าน่าน่า";

	vm.sections = [
		{
			"name":"eat",
			"title":"น่ากิน",
			"template": "preview/preview-img-left.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+"eat",
			"viewAll":true,
			"posts":[]
		},

		{ template:"hr.html"},
		{
			"name":"sleep",
			"title":"น่าพัก",
			"template": "preview/preview-img-left.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+"sleep",
			"viewAll":true,
			"posts":[]
		},

		{ template:"hr.html"},
		{
			"name":"travel",
			"title":"น่าเที่ยว",
			"template": "preview/preview-img-left.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+"travel",
			"viewAll":true,
			"posts":[]
		},

		{ template:"hr.html"},
		{
			"name":"fun",
			"title":"น่าสนุก",
			"template": "preview/preview-img-left.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+"fun",
			"viewAll":true,
			"posts":[]
		},

	];

	postAPI.getRecommendedEachCategory()
	.success(function(resp){
		Global.initialComplete = true;

		var posts = {
			"eat":[],
			"sleep":[],
			"travel":[],
			"fun":[],
		}
		angular.forEach(resp.data,function(post){
			postTranform.toImageBg(post);
			postTranform.addCredit(post);
			postTranform.addLink(post);
			posts[post.category_name].push(post);

		});

		angular.forEach(vm.sections,function(section){
			if(!section.hasOwnProperty("name")){
				return false;
			}

			section.posts = posts[section.name];
		})
	});
	


}]);