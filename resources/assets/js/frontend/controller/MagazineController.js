angular.module('neekrungApp')
.controller('magazineController', ["Global","ROUTE_NAME","$routeParams","URL_CONSTANT","Notice","$scope","$location",function(Global,ROUTE_NAME,$routeParams,URL_CONSTANT,Notice,$scope,$location) {
	var vm = this;

	Global.selectedMenu=null;
	Global.initialComplete=true;

	vm.magazineSpine = 'Vol. 25 / December';
	
	$('#documentViewer').FlexPaperViewer(
            { config : {

                // SWFFile : 'docs/Paper.pdf.swf',
                // IMGFiles : 'demoPDF/demo/{page}.jpg',
                // JSONFile : 'demoPDF/Paper.pdf_10.js',
                IMGFiles : 'demoPDF/demo.pdf_{page}.png',
                JSONFile : 'demoPDF/demo.pdf_{page}.js',
                PDFFile : 'demoPDF/demo.pdf_[*,2].pdf',
                ThumbIMGFiles : 'demoPDF/demo.pdf_{page}_res_200.jpg',
                HighResIMGFiles : 'demoPDF/demo.pdf_{page}_res_.jpg',


                Scale : 1.0,
                ZoomTransition : 'easeOut',
                ZoomTime : 0.5,
                ZoomInterval : 0.1,
                FitPageOnLoad : true,
                FitWidthOnLoad : false,
                FullScreenAsMaxWindow : false,
                ProgressiveLoading : false,
                MinZoomSize : 0.2,
                MaxZoomSize : 5,
                SearchMatchAll : false,
                PrintPaperAsBitmap:false,

                RenderingOrder : 'html5,html',

                EnableWebGL : true,
                ViewModeToolsVisible : true,
                ZoomToolsVisible : true,
                NavToolsVisible : true,
                CursorToolsVisible : true,
                SearchToolsVisible : true,
                WMode : 'transparent',
                localeChain: 'en_US'
            }}
    );

}]);