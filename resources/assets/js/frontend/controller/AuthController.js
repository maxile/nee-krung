angular.module('neekrungApp')
.controller('authController', ["Global","Notice","userAPI","ROUTE_NAME","URL_CONSTANT","facebookAPI","$scope","$location","$window",function(Global,Notice,userAPI,ROUTE_NAME,URL_CONSTANT,facebookAPI,$scope,$location,$window) {
	var vm = this;
	vm.userObj = {};

	vm.readyToFB = false;
	vm.requestSenging = false;

	vm.fbLogin = fbLogin;
	vm.linkTo = linkTo;

	vm.login = login;
	vm.logout = logout;
	vm.signup = signup;
	vm.init = init;

	function init(){
		Global.selectedMenu="member";
		Global.initialComplete=true;		
	}
	

	userAPI.getUser()
	.success(function(resp){
		$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER);
	})

	$scope.$watch(function(){
		return facebookAPI.isReady();
	},function(){
		vm.readyToFB = true;
	});

	function fbLogin(){
		vm.requestSenging = true;
		facebookAPI.login()
		.then(function(resp){
			$window.location.reload();
		},function(){
			$window.location.reload();
		});	
	}

	function linkTo(path){
		$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER+path)
	}

	function logout(){
		userAPI.logout()
		.then(function(resp){
			$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER_LOGIN)
		})
	}

	function login(){
		vm.requestSenging = true;
		userAPI.login()
		.then(function(resp){
			$window.location.reload();
		},function(resp){
			$scope.message = [resp.data.message];
			Notice.warning($scope)
			.result.finally(function(){
				$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER)
				// $window.location.reload();
			});
		})
	}

	function signup(){
		vm.requestSenging = true;
		userAPI.signup(vm.userObj)
		.then(function(resp){
			$scope.message = [resp.data.message];

			Notice.notice($scope)
			.result.finally(function() {
			    // $window.location.reload();
			    $location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER);
			});
			
		},function(resp){
			$scope.message = [];
			angular.forEach(resp.data.message,function(message){
				$scope.message.push(message[0]);
			})
			Notice.alert($scope);
		})
	}
	
}]);