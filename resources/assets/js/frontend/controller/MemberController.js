angular.module('neekrungApp')
.controller('memberController', ["Notice","userAPI","$scope","$location","$window","$routeParams","ROUTE_NAME","URL_CONSTANT","Global","postAPI","postTranform",function(Notice,userAPI,$scope,$location,$window,$routeParams,ROUTE_NAME,URL_CONSTANT,Global,postAPI,postTranform) {
	var vm = this;
	vm.user = null;

	Global.selectedMenu="member";
	Global.initialComplete=false;


	vm.postForm = false;
	vm.newPost = {
		contents:[]
	};
	vm.submitPost = submitPost;
	vm.canclePost = canclePost;

	vm.myPost = {
		"title":"My Article",
		"template": "preview/preview-3-col-extra.html",
		"newPostBtn" : true,
		"newPost" : newPostAction,
		"posts":[]
	}

	userAPI.getUser($routeParams.userID)
	.success(function(resp){
		vm.user = resp.data;
		Global.initialComplete=true;

	}).error(function(resp){

		if(!$routeParams.userID){
			$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER_LOGIN);
		}else{
			$scope.message = [resp.message];
			Notice.alert($scope)
			.result.finally(function(){
				$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.HOME);
			})	
		}
		
	});


	postAPI.getPostByUsername($routeParams.userID)
	.success(function(resp){
		resp = {"message":"Get your data complete.","data":[{"_id":"56af1cedbffebcc72c8b4567","section_name":"wlm","category":"World Longest Magazine","title":"\u0e40\u0e01\u0e47\u0e1a\u0e01\u0e25\u0e49\u0e2d\u0e07\u0e44\u0e27\u0e49\u0e43\u0e19\u0e01\u0e23\u0e30\u0e40\u0e1b\u0e4b\u0e32 \u0e2b\u0e22\u0e34\u0e1a\u0e21\u0e37\u0e2d\u0e16\u0e37\u0e2d\u0e02\u0e2d\u0e07\u0e40\u0e23\u0e32\u0e2d\u0e2d\u0e01\u0e44\u0e1b\u0e16\u0e48\u0e32\u0e22\u0e23\u0e39\u0e1b\u0e01\u0e31\u0e19","image_url":"images\/demo1-bnO2U.jpg","short_detail":"***","contents":[{"templateCode":"img","title":"","text":["",""],"image_url":"images\/demo1-gPluy.jpg","template":"post\/col-image.html"},{"templateCode":"text","title":"","text":["<p>\u0e2a\u0e27\u0e31\u0e2a\u0e14\u0e35\u0e04\u0e23\u0e31\u0e1a\u0e40\u0e1e\u0e37\u0e48\u0e2d\u0e19 \u0e46 \u0e01\u0e23\u0e30\u0e17\u0e39\u0e49\u0e19\u0e35\u0e49\u0e40\u0e1b\u0e47\u0e19\u0e01\u0e23\u0e30\u0e17\u0e39\u0e49\u0e17\u0e35\u0e48 3 \u0e02\u0e2d\u0e07\u0e40\u0e23\u0e32\u0e17\u0e35\u0e48\u0e42\u0e1e\u0e2a\u0e40\u0e01\u0e35\u0e48\u0e22\u0e27\u0e01\u0e31\u0e1a\u0e01\u0e32\u0e23\u0e17\u0e48\u0e2d\u0e07\u0e40\u0e17\u0e35\u0e48\u0e22\u0e27<br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/><br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/>\u0e41\u0e25\u0e30\u0e17\u0e35\u0e48\u0e17\u0e35\u0e48\u0e1c\u0e21\u0e44\u0e1b\u0e01\u0e47\u0e04\u0e37\u0e2d &#34; \u0e2b\u0e31\u0e27\u0e2b\u0e34\u0e19 , \u0e1b\u0e23\u0e32\u0e13\u0e1a\u0e38\u0e23\u0e35 &#34; \u0e40\u0e21\u0e37\u0e2d\u0e07\u0e1e\u0e31\u0e01\u0e1c\u0e48\u0e2d\u0e19\u0e15\u0e32\u0e01\u0e2d\u0e32\u0e01\u0e32\u0e28\u0e22\u0e2d\u0e14\u0e19\u0e34\u0e22\u0e21 \u0e1b\u0e01\u0e15\u0e34\u0e41\u0e25\u0e49\u0e27\u0e40\u0e27\u0e25\u0e32\u0e44\u0e1b\u0e40\u0e17\u0e35\u0e48\u0e22\u0e27<br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/><br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/>\u0e1c\u0e21\u0e01\u0e47\u0e08\u0e30\u0e40\u0e2d\u0e32 DSLR \u0e44\u0e1b\u0e16\u0e48\u0e32\u0e22\u0e23\u0e39\u0e1b\u0e15\u0e25\u0e2d\u0e14 \u0e41\u0e15\u0e48\u0e04\u0e23\u0e31\u0e49\u0e07\u0e19\u0e35\u0e49\u0e19\u0e36\u0e01\u0e2a\u0e19\u0e38\u0e01\u0e04\u0e23\u0e31\u0e1a \u0e2d\u0e22\u0e32\u0e01\u0e43\u0e0a\u0e49\u0e41\u0e04\u0e48\u0e01\u0e25\u0e49\u0e2d\u0e07\u0e21\u0e37\u0e2d\u0e16\u0e37\u0e2d \u0e2d\u0e22\u0e32\u0e01\u0e23\u0e39\u0e49\u0e27\u0e48\u0e32\u0e08\u0e30\u0e44\u0e14\u0e49\u0e20\u0e32\u0e1e\u0e14\u0e35 \u0e46 \u0e1a\u0e49\u0e32\u0e07\u0e44\u0e2b\u0e21<br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/><br style=\"color: rgb(51, 51, 51);text-align: center;background-color: rgb(255, 255, 255);\"\/>\u0e21\u0e32\u0e14\u0e39\u0e01\u0e31\u0e19\u0e40\u0e25\u0e22\u0e04\u0e23\u0e31\u0e1a\u0e27\u0e48\u0e32\u0e17\u0e23\u0e34\u0e1b\u0e19\u0e35\u0e49 \u0e01\u0e31\u0e1a\u0e21\u0e37\u0e2d\u0e16\u0e37\u0e2d\u0e41\u0e04\u0e48\u0e40\u0e04\u0e23\u0e37\u0e48\u0e2d\u0e07\u0e40\u0e14\u0e35\u0e22\u0e27\u0e1c\u0e21\u0e44\u0e14\u0e49\u0e23\u0e39\u0e1b\u0e2d\u0e30\u0e44\u0e23\u0e21\u0e32\u0e1a\u0e49\u0e32\u0e07<br\/><\/p>",""],"template":"post\/col-text.html"}],"story_by":null,"image_by":null,"published":true,"updated_at":"2016-02-01 08:53:01","created_at":"2016-02-01 08:53:01"},{"_id":"56af27aebffebc9a2c8b4568","section_name":"wlm","category":"World Longest Magazine","title":"\u0e40\u0e01\u0e47\u0e1a\u0e01\u0e25\u0e49\u0e2d\u0e07\u0e44\u0e27\u0e49\u0e43\u0e19\u0e01\u0e23\u0e30\u0e40\u0e1b\u0e4b\u0e32","image_url":"images\/1-szCnU.jpg","short_detail":"***","contents":[],"story_by":null,"image_by":null,"published":true,"updated_at":"2016-02-01 09:40:22","created_at":"2016-02-01 09:38:54"},{"_id":"56af2816bffebc992c8b4569","section_name":"wlm","category":"World Longest Magazine","title":"\u0e40\u0e01\u0e47\u0e1a\u0e01\u0e25\u0e49\u0e2d\u0e07\u0e44\u0e27\u0e49\u0e43\u0e19\u0e01\u0e23\u0e30\u0e40\u0e1b\u0e4b\u0e32","image_url":"images\/2-AiNyb.jpg","short_detail":"***","contents":[],"story_by":null,"image_by":null,"published":true,"updated_at":"2016-02-01 09:40:38","created_at":"2016-02-01 09:40:38"}]};
		vm.myPost.posts = resp.data;

		angular.forEach(resp.data,function(post){
			postTranform.addCredit(post);
			postTranform.toImageBg(post);
		});

	});


	
	function newPostAction(){
		vm.postForm = true;
	}

	function submitPost(){
		console.log("submit");
	}


	function canclePost(){
		vm.postForm = false;
		vm.newPost = {
			contents:[]
		}
	}

	
}]);