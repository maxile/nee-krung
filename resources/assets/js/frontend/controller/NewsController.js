angular.module('neekrungApp')
.controller('newsController', ["URL_CONSTANT","Global","postAPI","postTranform",function(URL_CONSTANT,Global,postAPI,postTranform) {
	var vm = this;
	Global.selectedMenu="news";
	Global.initialComplete=false;
	
	vm.title="Activities";
	vm.subTitle="หนีกรุง...ปรุงพิเศษ";
	vm.articles=[];
	

	postAPI.getPostBySection("news")
	.success(function(resp){
		Global.initialComplete = true;
		angular.forEach(resp.data,function(post){

			postTranform.toImageBg(post);
			postTranform.addCredit(post);
			postTranform.addLink(post);
			
			var article = {
				"template": "preview/preview-img-left.html",
				"posts":[ post]
			}
			
			vm.articles.push(article);
		});
	});
	


}]);