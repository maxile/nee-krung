angular.module('neekrungApp')
.controller('postController', ["Global","postAPI","postTranform","ROUTE_NAME","$routeParams","URL_CONSTANT","Notice","$scope","$location",function(Global,postAPI,postTranform,ROUTE_NAME,$routeParams,URL_CONSTANT,Notice,$scope,$location) {
	var vm = this;

	Global.selectedMenu=null;
	Global.initialComplete=false;

	vm.post = {};
	postAPI.getPostByID($routeParams.postName)
	.success(function(resp){
		Global.initialComplete = true;
		vm.post = resp.data;
		postTranform.addCredit(vm.post);

	}).error(function(resp){
		$scope.message = resp.message;
		Notice.alert($scope)
		.result.finally(function(){
			$location.path(URL_CONSTANT.ROOT_PATH);
		});
	});

}]);