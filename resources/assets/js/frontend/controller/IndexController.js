angular.module('neekrungApp')
.controller('indexController', ["$window","$scope","URL_CONSTANT","postAPI","ngImage","Global","postTranform","ROUTE_NAME",function($window,$scope,URL_CONSTANT,postAPI,ngImage,Global,postTranform,ROUTE_NAME) {
	var vm = this;
	
	vm.ngImage = ngImage;
	vm.banners = [];
	vm.magazine = null;

	Global.initialComplete=false;
	Global.selectedMenu=null;
	Global.navbarAnimate=true;

	vm.sections = [
		{
			"name":"recommended",
			"title":"Recommended",
			"template": "preview/preview-img-left.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED,
			"posts":[]
		},

		{ template:"hr.html"},
		{
			"name":"city-detox",
			"title":"City Detox",
			"template": "preview/preview-3-col.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.CITY_DETOX,
			"posts":[]
		},
		
		{
			"name":"wlm",
			"title":"World Longest Magazine",
			"template": "preview/preview-3-col-extra.html",
			"link": URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM,
			"posts":[]
		},
		{ template:"hr.html"},
		{
			"name":"news",
			"template": "preview/preview-img-left.html",
			"posts":[]
		},
		{ template:"hr.html"},
		{
			"name":"member",
			"template": "preview/preview-img-left.html",
			"posts":[
				{
					"image":{
						image_url:"/image/article/demo1.jpg"
					} ,
					"category":"Member",
					"title": "สมัครสมาชิก...รีบคลิกเลย",
					"link": ROUTE_NAME.MEMBER,
				}
			]
		},
		{ template:"hr.html"},

	];

	postAPI.getIndex()
	.success(function(resp){
		Global.initialComplete = true;
		vm.magazine = resp.magazine;
		postTranform.toImageBg(vm.magazine);

		angular.forEach(vm.sections,function(section){
			if(!section.hasOwnProperty("name")){
				return false;
			}

			if(resp.sections.hasOwnProperty( section.name)){
				section.posts = resp.sections[section.name].posts;
			}

			if(section.hasOwnProperty("posts")){
				angular.forEach(section.posts,function(post){
					postTranform.toImageBg(post);
					postTranform.addCredit(post);
					postTranform.addLink(post);
						

				});
			}
		})
	})
	
	vm.books = [
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/1.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/2.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/3.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/4.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/5.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/6.jpg'},
		{imageURL: URL_CONSTANT.HOST_NAME+ '/image/book/7.jpg'},
	]

	vm.nBook = 6;	
	vm.getNBook = getNBook;

	function getNBook(width){
		vm.nBook = Math.floor((width-30)/140);
		if(vm.nBook > 6){
			vm.nBook = 6;
		}
	}
	

}]);