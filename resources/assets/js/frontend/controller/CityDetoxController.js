angular.module('neekrungApp')
.controller('cityDetoxController', ["URL_CONSTANT","Global","postAPI","postTranform","$uibModal","$routeParams","$location","ROUTE_NAME",function(URL_CONSTANT,Global,postAPI,postTranform,$modal,$routeParams,$location,ROUTE_NAME) {
	var vm = this;

	Global.selectedMenu="city-detox";
	Global.initialComplete=false;

	vm.title="City Detox";
	vm.subTitle="";
	vm.posts=[];
	vm.box = box;

	postAPI.getPostBySection("city-detox")
	.success(function(resp){
		Global.initialComplete = true;
		vm.posts = resp.data;
		angular.forEach(resp.data,function(post){
			postTranform.imageURL(post);
			postTranform.toImageBg(post);
			postTranform.addLink(post);
		});
	});

	if($routeParams.postName){
		postAPI.getPostByID($routeParams.postName)
		.success(function(resp){
			box(resp.data);
		})	
	}
	

	function box(post){
		var path = URL_CONSTANT.ROOT_PATH+ROUTE_NAME.CITY_DETOX+post.route_name;
		$location.path(path,false);
		return $modal.open({
			animation:true,
	    	templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/modal/city-detox.html",
		    	controller:"cityDetoxPostController",
		    	controllerAs:"ctrl",
		      	size: "md",
		      	windowClass:"modal-city-detox",
		      	resolve:{
		      		post: function(){
		      			return post;
		      		}
		      	}
	    }).result.finally(function(){
	    	$location.path(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.CITY_DETOX,false);
	    })
	}

}]);