angular.module('neekrungApp')
.controller('aboutController', ["URL_CONSTANT","Global",function(URL_CONSTANT,Global) {
	var vm = this;
	vm.title="About Us";
	vm.subTitle="คือ..หนีกรุง ไปปรุงฝัน";
	vm.getInfoClass = getInfoClass;
	Global.selectedMenu="about-us";
	Global.initialComplete=true;
	
	vm.member=[
		[ 
			{imageURL: URL_CONSTANT.HOST_NAME+"/image/editor/1.png"},
			{userInfo: ["Rathrong Srilert","รัฐรงค์ ศรีเลิศ","nok5151@yohoo.com"]},
			{}
		],
		[
			{},
			{imageURL: URL_CONSTANT.HOST_NAME+"/image/editor/2.png"},
			{
				userInfo: ["Sittha Sivasen","สิฏฐะ ศิวเสน","thailand_j@yohoo.com"],
				education:["Marketing","Dhurakijpundit University","Bangkok, Thailand"]

			}
		],
		[ 
			{userInfo: ["Atichart Saitongin","อติชาติ สายทองอินทร์","chongsky1962@yohoo.com"]},
			{imageURL:URL_CONSTANT.HOST_NAME+"/image/editor/3.png"},
			{}
		],
		[ 
			{imageURL:URL_CONSTANT.HOST_NAME+"/image/editor/4.png"},
			{},
			{
				userInfo: ["Chorchai Teardkeatkul","ชอชาย เทอดเกียรติกุลlupang","lupang_1960@yohoo.com"],
				education:["Painting","Pohchang Academy of arts","Bangkok, Thailand"]
			},
			
		],
		// [ 
		// 	{imageURL: URL_CONSTANT.HOST_NAME+"image/editor/5.png"},
		// 	{
		// 		userInfo: [ "Chanatpon Whangperm","ชนัตพล หวังเพิ่ม","alessandro_golf@hotmail.com"],
		// 		education:["Communication Arts","Rangsit University","Bangkok, Thailand"]
		// 	},
		// 	{}
		// ],
		// [ 
		// 	{
		// 		userInfo: ["Korrakit Pinsrisook","กรกฤษณ์ พิณศรีสุข","korrakit@neekrung.com"],
		// 		education:["Marketing","Dhurakijpundit University","Bangkok, Thailand"]
		// 	},
		// 	{imageURL: URL_CONSTANT.HOST_NAME+"image/editor/6.png"},
		// 	{}
		// ],
	]	

	function getInfoClass(info){
		if(info.hasOwnProperty("userInfo"))
			return "info-container";
		else if(info.hasOwnProperty("imageURL"))
			return "profile-container";
		else
			return "space-container";
	}
}]);