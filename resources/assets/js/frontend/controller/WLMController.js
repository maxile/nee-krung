angular.module('neekrungApp')
.controller('wlmController', ["URL_CONSTANT","Global","postAPI","postTranform","ROUTE_NAME","$location","$window","$timeout","$filter",function(URL_CONSTANT,Global,postAPI,postTranform,ROUTE_NAME,$location,$window,$timeout,$filter) {
	var vm = this;
	vm.title="World Longest Magazine";
	vm.subTitle="ปรุงฝันฉันให้สุด...ในโลก";

	Global.selectedMenu="wlm";
	Global.initialComplete=false;

	vm.posts = [];
	vm.tags = ["ท่องเที่ยว","เที่ยว","ประเทศไทย"];
	vm.magazine = null;
	initMagazine();

	postAPI.getPostBySection("wlm")
	.success(function(resp){
		Global.initialComplete = true;
		vm.posts = resp.data;
		angular.forEach(resp.data,function(post){
			post.story_by = "ฉันเขียนเอง";
			postTranform.addCredit(post);
			postTranform.imageURL(post);
			postTranform.addLink(post);
			addMagazinePage(post);
		});

		vm.magazine.turn("next");
	});


	function initMagazine() {

		vm.magazine = jQuery('.magazine');
		vm.magazine.turn({
			// width: width,
			height: 400,
			elevation: 50,
			gradients: true,
			autoCenter: true,
			when: {
				turned: function(event, page, view) {
					$(this).turn('center');
					if (page==1) { 
						$(this).turn('peel', 'br');
					}

				}
			}
		});

		vm.magazine.addClass('animated');

	}

	function addMagazinePage(post){
		var element = $('<div />');
		var gradient = $("<div/>").addClass("gradient");
		var content = $("<div/>").addClass("magazine-content");

		var inner = '';
		inner += '<div class="date">'+$filter('dateBt')(post.created_at)+'</div>'
		inner += '<div class="image">'
		inner += 	'<img src="'+post.imageURL+'">'
		inner += '</div>'
		inner += '<div class="big center">'
		inner += 	post.title
		inner += 	'<hr>'
		for (var i = 0; i < post.credit.length; i++) {
			inner += 	'<div class="small center">'+post.credit[i]+'</div>'	
		}
		
		inner += '</div>';

		content.html(inner);
		element.append(gradient);
		element.append(content);
		vm.magazine.turn('addPage', element);
	}


}]);




// loadApp();