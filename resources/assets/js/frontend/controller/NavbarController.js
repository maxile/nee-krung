angular.module('neekrungApp')
.controller('navbarController', ["ROUTE_NAME","URL_CONSTANT","$location","$window","Global","$scope",function(ROUTE_NAME,URL_CONSTANT,$location,$window,Global,$scope) {
	var vm = this;
	vm.moveTo = moveTo;
	vm.ROUTE_NAME = ROUTE_NAME;
	vm.logoURL = URL_CONSTANT.HOST_NAME+"/image/logo.png";

	vm.selectedMenu = null;
	vm.bigClass = false;

	$scope.$watch(function(){
		return Global.selectedMenu;
	},function(){
		vm.selectedMenu = Global.selectedMenu;
	})

	$scope.$watch(function(){
		return Global.navbarAnimate;
	},function(){
		if(Global.navbarAnimate){
			navbarAnimate($window.pageYOffset);
			angular.element($window).bind("scroll", function() {
	            var yOffset = this.pageYOffset;
	            navbarAnimate(yOffset);
	            $scope.$apply();
	        });
		}else{

			vm.bigClass = false;
			angular.element($window).unbind("scroll");
		}
	})


	function navbarAnimate(yOffset){
		if(yOffset<120){
			vm.bigClass = true;	
		}else{
			vm.bigClass = false;	
		}
		
		
	}

	function moveTo(key){
		var target={
			"home":ROUTE_NAME.HOME,
			"about-us":ROUTE_NAME.ABOUT_US,
			"recommended":ROUTE_NAME.RECOMMENDED,
			"city-detox":ROUTE_NAME.CITY_DETOX,
			"wlm":ROUTE_NAME.WLM,
			"news":ROUTE_NAME.NEWS,
			"member":ROUTE_NAME.MEMBER_LOGIN
		}

		if(target.hasOwnProperty(key)){
			Global.selectedMenu = null;
			Global.navbarAnimate = false;
			$location.path(URL_CONSTANT.ROOT_PATH+target[key]);
		}
	}
}]);