angular.module('neekrungApp')
.filter('dateBt', function(){
  return function(input) {
    var d = new Date(input);
	var year = d.getFullYear();
	var month = d.getMonth();
	var date = d.getDate();
	
	if(date<=9){
		date = "0"+date;
	}

	if(month<=9){
		month = "0"+month;
	}

	return "-"+date+"-"+month+"-"+year+"-";
  };
});