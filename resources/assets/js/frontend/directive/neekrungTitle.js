angular.module('neekrungApp')
.directive('neekrungTitle', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"myTitle":"=",
	    	"link":"=",
	    	"viewAll":"=",
	    	"nextPost":"&",
	    	"prevPost":"&",
	    	"len":"="
	    },
	    templateUrl : URL_CONSTANT.HOST_NAME+"/template/fragment/article-header.html",
	    link: function (scope, element) {
	    	scope.nextable = true;
	    	scope.prevable = false;

	    	if(scope.len==1){
	    		scope.nextable = false;
	    	}

	    	scope.next = function(){
	    		var n = scope.nextPost();
	    		if(n==scope.len-1){
	    			scope.nextable = false;
	    		}
	    	}

	    	scope.prev = function(){
	    		var n = scope.prevPost();
	    		if(n==0){
	    			scope.prevable = false;
	    		}
	    	}
	    }
	};
}]);