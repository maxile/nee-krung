angular.module('neekrungApp')
.directive('facebookShare',["$window","Facebook",function ($window,Facebook) {
    return {
        restrict: 'A',
        scope: {
            facebookShare:"="
        },
        link: function(scope, element, attrs) {
            scope.$watch(function(){
                return Facebook.isReady();
            },function(){

                element.bind('click', function($event) {
                    Facebook.ui({
                      method: 'share',
                      href: scope.facebookShare,
                    }, function(response){

                    });
                });
                

            });
            
        }
    };
}]);