angular.module('neekrungApp')
.directive('square', ["$timeout","$window",function($timeout,$window) {
  	return {
	    restrict: 'A',
	    scope: {},
	    link:function(scope,element){
	    	var scopeApplyContainer;
	    	scope.getWidth = function () {
				return $(element).width();
	        };

	        scope.$watch(scope.getWidth, function (width) {
	        	$(element).height(width)
	        	// console.log( )
	        	// element.height(width);
	        },true);

	        angular.element($window).bind('resize', function () {
	        	$timeout.cancel(scopeApplyContainer);
	        	scopeApplyContainer = $timeout(function(){scope.$apply();}, 5, false);
	        });
	    }
	};
}]);