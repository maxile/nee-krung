angular.module('neekrungApp')
.directive('neekrungAboutHeader', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"myTitle":"=",
	    	"myContext":"=",
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/about-header.html"
	};
}]);