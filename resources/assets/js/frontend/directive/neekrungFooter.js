angular.module('neekrungApp')
.directive('neekrungFooter', ["URL_CONSTANT",'$anchorScroll',"$location",function(URL_CONSTANT,$anchorScroll,$location) {
  	return {
	    restrict: 'E',
	    scope:{
	    	"sponsor":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/footer.html",
	    link:function(scope){
	    	scope.gotoTop = function(){
	    		$location.hash('top');
		      	$anchorScroll();
	    	}
	    }

	};
}]);