angular.module('neekrungApp')
.directive('facebookCommentBox',["$window","Facebook",function ($window,Facebook) {
    function createHTML(href, numposts) {
        var html = '<div class="fb-comments" ' +
                       'data-href="' + href + '" ' +
                       "data-width='700' "+
                       'data-numposts="' + numposts + '" ' + '>' +
               '</div>';
        return html;
    }

    return {
        restrict: 'E',
        scope: {
            fbHref:"="
        },
        link: function(scope, elem, attrs) {
            scope.$watch(function(){
                return Facebook.isReady();
            },function(){

                if(Facebook.isReady()){
                    elem.html(createHTML(scope.fbHref, 5));
                    Facebook.parseXFBML(elem[0]);
                }
            });
            
        }
    };
}]);