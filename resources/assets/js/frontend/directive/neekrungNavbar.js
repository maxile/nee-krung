angular.module('neekrungApp')
.directive('neekrungNavbar', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"animate":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/navbar.html"
	};
}]);