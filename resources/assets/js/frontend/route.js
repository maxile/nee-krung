angular.module('neekrungApp')
.constant('ROUTE_NAME',{
    "HOME": "",
    "POST": "p/",
    // "HIGHLIGHT": "highlight/",
    "RECOMMENDED": "recommended/",
    "CITY_DETOX": "city-detox/",

    "WLM_POST": "w/",
    "WLM": "world-longest-magazine/",
    // "CALENDAR": "calendar/",
    "NEWS": "news/",
    // "TV": "neekrung-tv/",
    "MEMBER_LOGIN": "member/login/",
    "MEMBER_SIGNUP": "member/signup/",
    "MEMBER": "member/",
    "MAGAZINE": "magazine/",

    "ABOUT_US": "about-us/",
    "CONTACT_US": "contact-us/",

})
.config(["$routeProvider","$locationProvider","URL_CONSTANT","ROUTE_NAME",function($routeProvider,$locationProvider,URL_CONSTANT,ROUTE_NAME) {
    $routeProvider
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.HOME, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/index.html',
            controller  : 'indexController',
            controllerAs: 'ctrl'
        })
        // .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.POST+":postName", {
        //     templateUrl : URL_CONSTANT.HOST_NAME+'/template/post.html',
        //     controller  : 'postController',
        //     controllerAs: 'ctrl'
        // })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/recommended.html',
            controller  : 'recommendedController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+":category", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/recommended-page.html',
            controller  : 'recommendedCategoryController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.RECOMMENDED+":category/:postName", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/post.html',
            controller  : 'postController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.CITY_DETOX, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/city-detox.html',
            controller  : 'cityDetoxController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.CITY_DETOX+":postName", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/city-detox.html',
            controller  : 'cityDetoxController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.NEWS, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/page.html',
            controller  : 'newsController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.NEWS+":postName", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/post.html',
            controller  : 'postController',
            controllerAs: 'ctrl'
        })
        // .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM_POST+":postName", {
        //     templateUrl : URL_CONSTANT.HOST_NAME+'/template/wlm-post.html',
        //     controller  : 'wlmPostController',
        //     controllerAs: 'ctrl'
        // })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/wlm.html',
            controller  : 'wlmController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.WLM+":postName", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/post.html',
            controller  : 'postController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.ABOUT_US, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/about.html',
            controller  : 'aboutController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER_LOGIN, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/member-login.html',
            controller  : 'authController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER_SIGNUP, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/member-signup.html',
            controller  : 'authController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER, {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/member.html',
            controller  : 'memberController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MEMBER+":userID", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/member.html',
            controller  : 'memberController',
            controllerAs: 'ctrl'
        })
        .when(URL_CONSTANT.ROOT_PATH+ROUTE_NAME.MAGAZINE+":magazineVol", {
            templateUrl : URL_CONSTANT.HOST_NAME+'/template/magazine.html',
            controller  : 'magazineController',
            controllerAs: 'ctrl'
        })
    $locationProvider.html5Mode(true);
}]);