angular.module('neekrungApp')
.filter('arr', function(){
  return function(input, total) {
    total = Math.ceil(parseFloat(total));
    for (var i=0; i<total; i++)
      input.push(i);
    return input;
  };
});