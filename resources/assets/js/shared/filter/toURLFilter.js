angular.module('neekrungApp')
.filter("toURL", ['$sce', function($sce) {
  return function(url){
    return $sce.trustAsResourceUrl(url);
  }
}]);