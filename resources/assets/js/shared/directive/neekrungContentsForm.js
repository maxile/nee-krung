angular.module('neekrungApp')
.directive('neekrungContentsForm', ["URL_CONSTANT","ContentBox",function(URL_CONSTANT,ContentBox) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"ngModel":"=",
	    	"templateList":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/contentForm.html",
	    link:function(scope,element, attributes){
	    	scope.box = function(content){

	    		var createState = true;
	    		if(content){
	    			createState = false;
	    		}

				ContentBox.show(content,scope.templateList)
				.result.then(function(box){
			    	if(box.templateCode=="img"){
			    		box.template = "post/col-image.html";
			    	}else if(box.templateCode=="text"){
			    		box.template = "post/col-text.html";
			    	}else if(box.templateCode=="text-text"){
			    		box.template = "post/col-text-text.html";
			    	}else if(box.templateCode=="img-text"){
			    		box.template = "post/col-image-text.html";
			    	}else if(box.templateCode=="text-img"){
			    		box.template = "post/col-text-image.html";
			    	}else if(box.templateCode=="text-text-extra"){
			    		box.template = "post/col-text-text-extra.html";
			    	}

			    	if(createState){
			    		scope.ngModel.push(box);	
			    	}
			    	
			    })
			}

			scope.deleteContent = function(index){
				scope.ngModel.splice(index,1);
			}

			scope.swap = function(a,b){
				if(a<0){
					return false;
				}

				if(b>=scope.ngModel.length){
					return false;
				}

				

				var tmp = scope.ngModel[a];
				scope.ngModel[a] = scope.ngModel[b]; 
				scope.ngModel[b] = tmp;
				return true;
			}
	    },
	};
}]);