angular.module('neekrungApp')
.directive('neekrungPageHeader', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"myTitle":"=",
	    	"subTitle":"=",
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/page-header.html"
	};
}]);