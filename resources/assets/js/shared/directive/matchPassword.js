angular.module('neekrungApp')
.directive("match", function() {
    return {
        require: "ngModel",
        scope: {
            target: "=match"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.match = function(modelValue) {
                return modelValue == scope.target;
            };
 
            scope.$watch("target", function() {
                ngModel.$validate();
            });
        }
    };;
})