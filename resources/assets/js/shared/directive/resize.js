angular.module('neekrungApp')
.directive('resize', ["$window",function($window) {
  	return {
	    restrict: 'A',
	    scope: {
	    	"resize":"&"
	    },
	    link: function (scope, element) {
	    	scope.resize({width:element[0].clientWidth});
	    	angular.element($window).bind('resize', function(){
		  		scope.resize({width:element[0].clientWidth});
		     	scope.$apply();
		 	});

	    }
	};
}]);