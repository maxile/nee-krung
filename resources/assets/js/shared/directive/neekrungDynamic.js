angular.module('neekrungApp')
.directive('neekrungDynamic', ["URL_CONSTANT","$sce","$location","ngImage","Global",function(URL_CONSTANT,$sce,$location,ngImage,Global) {
	return {
	    template: '<ng-include src="getTemplateUrl()"/>',
	    scope:{
	    	"info":"=",
	    	"template":"="
	    },
	    restrict: 'E',
	    link: function(scope) {
	    	scope.current = 0;

	    	scope.nextPost = function(){
	    		scope.current++;
	    		if(scope.current>= scope.info.posts.length){
	    			scope.current = 0;
	    		}

	    		return scope.current;
	    	}

	    	scope.prevPost = function(){
	    		scope.current--;
	    		if(scope.current< 0){
	    			scope.current = scope.info.posts.length-1;
	    		}
	    		return scope.current;
	    	}

	      	scope.getTemplateUrl = function() {
	      		if(scope.template)
	      			return URL_CONSTANT.HOST_NAME+"/template/fragment/"+scope.template;
	      		
	        	return URL_CONSTANT.HOST_NAME+"/template/fragment/"+scope.info.template;
	      	}

	      	scope.textAlignByLength = function(text){
	      		if(!text){
	      			return {};
	      		}
	      		
	      		if(text.length>200){
	      			return {"text-align":"justify"}
	      		}else{
	      			return {"text-align":"center"}
	      		}
	      	}
	      	scope.linkTo = function(link){
	      		Global.initialComplete=false;
				Global.selectedMenu=null;
				Global.navbarAnimate=false;

	      		$location.path(link);
	      	}
	      	
			scope.imageURL = function(image){
				return ngImage.imageURL(image);
	    	}

	    	scope.fullURL = function(url){
	    		return URL_CONSTANT.HOST_NAME+url;
	    	}
	    }
	};
}]);