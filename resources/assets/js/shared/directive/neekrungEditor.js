angular.module('neekrungApp')
.directive('neekrungEditor', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"ngModel":"=",
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/editor.html",
	};
}]);