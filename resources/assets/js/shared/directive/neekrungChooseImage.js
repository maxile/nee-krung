angular.module('neekrungApp')
.directive('neekrungChooseImage', ["URL_CONSTANT","uploadAPI","Notice","ngImage",function(URL_CONSTANT,uploadAPI,Notice,ngImage) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"ngModel":"=",
	    	"helper":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/choose-image.html",
	    link:function(scope,element, attributes){

	    	scope.preview = false;
	    	scope.uploadding = false;
	    	scope.previewImage = null;

	    	var imageForm = null;
	    	scope.imageURL = function(image){
	    		return ngImage.imageURL(image);
	    	}

	    	scope.cancle = function(){
	    		scope.previewImage = null;
                scope.preview = false;
                imageForm.value="";
	    	}


	    	scope.upload = function(){
	    		var fd = new FormData();
			    fd.append("neekrungImage", imageForm.files[0]);
			    
			    scope.uploadding = true;
			    uploadAPI.image(fd)
			    .success(function(resp){
			    	scope.message = [resp.message];
			    	Notice.notice(scope);

			    	scope.uploadding = false;
			    	scope.previewImage = null;
                	scope.preview = false;
                	scope.ngModel = resp.data;
                	
			    }).error(function(resp){
			    	scope.message = resp.message["neekrungImage"];
			    	Notice.alert(scope);

			    	scope.uploadding = false;
			    });
	    	}

	    	element.bind("change", function (changeEvent) {
                var reader = new FileReader();
                reader.onload = function (loadEvent) {
                    scope.$apply(function () {
                    	scope.previewImage = loadEvent.target.result;
                    	scope.preview = true;
                    });
                }
                
            	imageForm = changeEvent.target;
            	if(imageForm.files[0].size >= 1000000){
            		scope.message = ["This file is very fat,Please resize your file."];
            		imageForm.value="";
			    	Notice.alert(scope);
            	}else{
            		reader.readAsDataURL(imageForm.files[0]);
            	}
            	
            	

                

                
            });
	    }
	};
}]);