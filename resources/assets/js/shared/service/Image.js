angular.module('neekrungApp')
.factory('ngImage', ["URL_CONSTANT",function(URL_CONSTANT){
	return {
		imageURL: function(image){
			return URL_CONSTANT.HOST_NAME+"/"+image;
		}
 	}
}]);