angular.module('neekrungApp')
.factory('Notice', ["URL_CONSTANT","$uibModal",function(URL_CONSTANT,$modal){
	function callModal(modalTemplate,size,scope){
		var messages = [];
		if(angular.isArray(scope.message)){
			for (var i = 0; i < scope.message.length; i++) {
				messages.push( scope.message[i]);
			};	
		}else if(angular.isObject(scope.message)){
			for(key in scope.message){
				for (var i = 0; i < scope.message[key].length; i++) {
					messages.push( scope.message[key][i]);
				};	
			}
		}else{
			messages.push(scope.message);
		}

		scope.message = messages;
		return $modal.open({
			animation:true,
	    	templateUrl: modalTemplate,
	      	size: size,
	      	scope:scope

	    });	
	}
	return {
		warning: function(scope){
			return callModal(URL_CONSTANT.HOST_NAME+'/template/fragment/modal/warning.html',"md",scope)
		},
		alert: function(scope){
			return callModal(URL_CONSTANT.HOST_NAME+'/template/fragment/modal/alert.html',"md",scope)
		},
		notice: function(scope){
			return callModal(URL_CONSTANT.HOST_NAME+'/template/fragment/modal/notice.html',"md",scope)
		},
 	}


}]);