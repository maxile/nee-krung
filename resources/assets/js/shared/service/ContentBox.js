angular.module('neekrungApp')
.factory('ContentBox', ["URL_CONSTANT","$uibModal",function(URL_CONSTANT,$modal){
	return {
		show:function(content,templateList){
			return $modal.open({
				animation:true,
		    	templateUrl: URL_CONSTANT.HOST_NAME+"/template/fragment/modal/create-box.html",
		    	controller:"contentBoxController",
		    	controllerAs:"ctrl",
		      	size: "lg",
		      	resolve:{
		      		content: function(){
		      			if(!content){
		      				return {
		      					"templateCode":"text",
		      					"title":"",
		      					"text":["",""]
		      				}
		      			}else{
		      				return content;	
		      			}
		      			
		      		},
		      		templateList: function(){
		      			if(!templateList){
		      				return [
		      					"text",
		      					"img",
		      					"text-text",
		      					"img-text",
		      					"text-img",
		      					"text-text-extra",
		      				]
		      			}else{
		      				return templateList;	
		      			}
		      		}
		      	}
		    })

		}
 	}


}]);