angular.module('neekrungApp')
.factory('uploadAPI', ["API","$http","URL_CONSTANT",function(API,$http,URL_CONSTANT){
	return {
		image:function(data){
			var url = URL_CONSTANT.API_PATH+API.IMAGE_UPLOAD;
			var options = {
		        withCredentials: true,
		        transformRequest: angular.identity,
		        headers: {'Content-Type': undefined}
		    };

			return $http.post(url, data, options);
		},
		pdf:function(data){
			var url = URL_CONSTANT.API_PATH+API.PDF_UPLOAD;
			var options = {
		        withCredentials: true,
		        transformRequest: angular.identity,
		        headers: {'Content-Type': undefined}
		    };

			return $http.post(url, data, options);
		},
 	}


}]);