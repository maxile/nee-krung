angular.module('neekrungApp')
.controller('contentBoxController', ["content","$uibModalInstance","templateList","URL_CONSTANT",function(content,$uibModalInstance,templateList,URL_CONSTANT){
	var vm = this;
	vm.content = content;
	vm.templateList = templateList;
	
	vm.rootPath = URL_CONSTANT.HOST_NAME;
	
	vm.submit = function(){
		$uibModalInstance.close(content);
	}

	vm.dismiss = function(){
		$uibModalInstance.dismiss();
	}


}]);