angular.module('neekrungApp')
.factory('memberAPI', ["API","$http","URL_CONSTANT",function(API,$http,URL_CONSTANT){
	return {
		login:function(data){
			return $http({
			    method: 'POST',
			    url: URL_CONSTANT.API_PATH+API.LOGIN,
			    data: data
			})
		},
		logout:function(){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+API.LOGOUT
			})
		},
		fetch:function(){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.MEMBER
			})
		},
		get:function(id){
			console.log(id);
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.MEMBER+id
			})
		},
		store:function(data){
			return $http({
			    method: 'POST',
			    url: URL_CONSTANT.API_PATH+API.MEMBER,
			    data:data
			})
		},
		update:function(id,data){
			return $http({
			    method: 'PUT',
			    url: URL_CONSTANT.API_PATH+API.MEMBER+id,
			    data:data
			})
		},
		delete:function(id){
			return $http({
			    method: 'DELETE',
			    url: URL_CONSTANT.API_PATH+API.MEMBER+id
			})
		},
 	}


}]);