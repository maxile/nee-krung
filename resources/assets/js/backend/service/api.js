angular.module('neekrungApp')
.constant('API',{
	MAGAZINE:"api/magazine/",
	POST:"api/post/",
	MEMBER:"api/member/",
	SECTION:"api/section/",

	LOGIN:"api/member/login",
	LOGOUT:"api/member/logout",
	IMAGE_UPLOAD: "api/upload/image/",
	PDF_UPLOAD: "api/upload/pdf/"
})