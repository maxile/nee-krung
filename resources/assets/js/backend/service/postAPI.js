angular.module('neekrungApp')
.factory('postAPI', ["API","$http","URL_CONSTANT",function(API,$http,URL_CONSTANT){
	return {
		fetch:function(){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.POST
			})
		},
		get:function(id){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.POST+id
			})
		},
		getBySection:function(sectionName){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.POST+"section/"+sectionName
			})
		},
		store:function(data){
			return $http({
			    method: 'POST',
			    url: URL_CONSTANT.API_PATH+API.POST,
			    data:data
			})
		},
		update:function(id,data){
			return $http({
			    method: 'PUT',
			    url: URL_CONSTANT.API_PATH+API.POST+id,
			    data:data
			})
		},
		delete:function(id){
			return $http({
			    method: 'DELETE',
			    url: URL_CONSTANT.API_PATH+API.POST+id
			})
		},

 	}


}]);