angular.module('neekrungApp')
.factory('magazineAPI', ["API","$http","URL_CONSTANT",function(API,$http,URL_CONSTANT){
	return {
		fetch:function(){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.MAGAZINE
			})
		},
		get:function(id){
			return $http({
			    method: 'GET',
			    url: URL_CONSTANT.API_PATH+ API.MAGAZINE+id
			})
		},
		store:function(data){
			return $http({
			    method: 'POST',
			    url: URL_CONSTANT.API_PATH+API.MAGAZINE,
			    data:data
			})
		},
		update:function(id,data){
			return $http({
			    method: 'PUT',
			    url: URL_CONSTANT.API_PATH+API.MAGAZINE+id,
			    data:data
			})
		},
		delete:function(id){
			return $http({
			    method: 'DELETE',
			    url: URL_CONSTANT.API_PATH+API.MAGAZINE+id
			})
		},

 	}


}]);