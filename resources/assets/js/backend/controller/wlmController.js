angular.module('neekrungApp')
.controller('wlmController', ["postAPI","$window","URL_CONSTANT","Notice","$scope","Global",function(postAPI,$window,URL_CONSTANT,Notice,$scope,Global) {
	var vm = this;
	Global.selectedMenu="wlm";
	vm.fetchPost = fetchPost;
	vm.posts = [];
	vm.newPost = {
		section_name:"wlm",
		category:"World Longest Magazine",
		short_detail:"***",
		contents:[]
	};

	vm.linkTo = linkTo;
	
	vm.initState = initState;
	vm.deletePost = deletePost;
	vm.submitPost = submitPost;

	vm.canSubmit = true;

	vm.setReturnLink = setReturnLink;
	vm.returnLink = null;

	function fetchPost(){
		postAPI.getBySection('wlm')
		.success(function(resp){
			vm.posts = resp.data;
			console.log(resp);
		});
	}

	function linkTo(url){
		$window.location.href=url;
	}

	function deletePost(index){
		if(index >= vm.posts.length){
			return false;
		}

		postAPI.delete(vm.posts[index]._id)
		.success(function(resp){
			vm.posts.splice(index,1);
		});
	}

	function initState(id){
		if(id!=""){
			vm.canSubmit = false;
			postAPI.get(id)
			.success(function(resp){
				vm.canSubmit = true;
				vm.newPost = resp.data;
			});
		}
	}

	function setReturnLink(link){
		vm.returnLink = link;
	}

	function submitPost(){
		vm.canSubmit = false;

		function noticeCallback(resp){
			vm.canSubmit = true;
			$scope.message = [resp.message];
			Notice.notice($scope)
			.result.finally(function(){
				if(vm.returnLink){
					linkTo(vm.returnLink);	
				}else{
					$window.location.reload();
				}
				
			})
		}

		function alertCallback(resp){
			vm.canSubmit = true;
			$scope.message = resp.message;
			Notice.alert($scope)
			// .result.finally(function(){
			// 	$window.location.reload();
			// })
		}

		console.log(vm.newPost);
		if( vm.newPost.hasOwnProperty("_id")){
			postAPI.update( vm.newPost._id, vm.newPost)
			.success(noticeCallback)
			.error(alertCallback)
		}else{
			postAPI.store(vm.newPost)
			.success(noticeCallback)
			.error(alertCallback)
		}
	}

}]);