angular.module('neekrungApp')
.controller('memberController', ["memberAPI","$window","URL_CONSTANT","Notice","$scope","Global",function(memberAPI,$window,URL_CONSTANT,Notice,$scope,Global) {
	var vm = this;
	Global.selectedMenu="member";

	vm.fetchMember = fetchMember;
	vm.members = [];
	vm.newMember = {};

	vm.linkTo = linkTo;

	vm.initState = initState;
	vm.editVersion = false;
	
	vm.deleteMember = deleteMember;
	vm.submitMember = submitMember;

	vm.canSubmit = true;

	vm.setReturnLink = setReturnLink;
	vm.returnLink = null;

	function fetchMember(){
		memberAPI.fetch()
		.success(function(resp){
			console.log(resp);
			vm.members = resp.data;
		});
	}

	function linkTo(url){
		$window.location.href=url;
	}

	function deleteMember(index){
		if(index >= vm.members.length){
			return false;
		}

		memberAPI.delete(vm.members[index]._id)
		.success(function(resp){
			vm.members.splice(index,1);
		});
	}

	function initState(id){
		if(id!=""){
			vm.canSubmit = false;
			vm.editVersion = true;

			memberAPI.get(id)
			.success(function(resp){
				vm.canSubmit = true;
				vm.newMember = resp.data;
				vm.newMember.password="";
			});
		}
	}

	function setReturnLink(link){
		vm.returnLink = link;
	}

	function submitMember(){
		vm.canSubmit = false;

		function noticeCallback(resp){
			vm.canSubmit = true;
			$scope.message = [resp.message];
			Notice.notice($scope)
			.result.finally(function(){
				if(vm.returnLink){
					linkTo(vm.returnLink);	
				}else{
					$window.location.reload();
				}
				
			})
		}

		function alertCallback(resp){
			vm.canSubmit = true;
			$scope.message = resp.message;
			Notice.alert($scope)
			// .result.finally(function(){
			// 	$window.location.reload();
			// })
		}

		if( vm.newMember.hasOwnProperty("_id")){
			memberAPI.update( vm.newMember._id, vm.newMember)
			.success(noticeCallback)
			.error(alertCallback)
		}else{
			memberAPI.store(vm.newMember)
			.success(noticeCallback)
			.error(alertCallback)
		}
	}

}]);