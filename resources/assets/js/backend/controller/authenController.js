angular.module('neekrungApp')
.controller('authenController', ["$scope","Notice","$window","memberAPI",function($scope,Notice,$window,memberAPI) {
	var vm = this;

	vm.login = login;
	vm.logout = logout;
	vm.member ={};

	function login(){

		var member = vm.member;
		vm.member ={};
		memberAPI.login(member)
		.success(function(resp){
			$window.location.reload();
		})
		.error(function(resp){
			$scope.message = [resp.message];
			Notice.alert($scope);

		})
	}


	function logout(){
		memberAPI.logout()
		.success(function(resp){
			$window.location.reload();
		})
	}
}]);