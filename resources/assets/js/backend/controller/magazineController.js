angular.module('neekrungApp')
.controller('magazineController', ["magazineAPI","$window","URL_CONSTANT","Notice","$scope","Global",function(magazineAPI,$window,URL_CONSTANT,Notice,$scope,Global) {
	var vm = this;
	Global.selectedMenu="magazine";

	vm.fetchMagazines = fetchMagazines;
	vm.magazines = [];
	vm.newMagazine = { banners:[{}]};

	vm.linkTo = linkTo;
	vm.initState = initState;
	vm.deleteMagazine = deleteMagazine;
	vm.submitMagazine = submitMagazine;
	
	vm.setReturnLink = setReturnLink;
	vm.returnLink = null;

	vm.canSubmit = true;

	function fetchMagazines(){
		magazineAPI.fetch()
		.success(function(resp){
			vm.magazines = resp.data;
			console.log(resp);
		});
	}

	function linkTo(url){
		$window.location.href=url;
	}

	function deleteMagazine(index){
		if(index >= vm.magazines.length){
			return false;
		}
		magazineAPI.delete(vm.magazines[index]._id)
		.success(function(resp){
			vm.magazines.splice(index,1);
		});
	}

	function initState(id){
		if(id!=""){
			vm.canSubmit = false;
			magazineAPI.get(id)
			.success(function(resp){
				vm.canSubmit = true;
				vm.newMagazine = resp.data;
			});
		}
	}

	function submitMagazine(){
		vm.canSubmit = false;
		console.log(vm.newMagazine);

		function noticeCallback(resp){
			vm.canSubmit = true;
			$scope.message = [resp.message];
			Notice.notice($scope)
			.result.finally(function(){
				if(vm.returnLink){
					linkTo(vm.returnLink);	
				}else{
					$window.location.reload();
				}
				
			})
		}

		function alertCallback(resp){
			vm.canSubmit = true;
			$scope.message = resp.message;
			Notice.alert($scope)
			// .result.finally(function(){
			// 	$window.location.reload();
			// })
		}

		if( vm.newMagazine.hasOwnProperty("_id")){
			magazineAPI.update( vm.newMagazine._id, vm.newMagazine)
			.success(noticeCallback)
			.error(alertCallback)
		}else{
			magazineAPI.store(vm.newMagazine)
			.success(noticeCallback)
			.error(alertCallback)
		}
	}

	function setReturnLink(link){
		vm.returnLink=link;
	}
}]);