angular.module('neekrungApp')
.directive('selectMonth', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"model":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/selectMonth.html",
	    link : function(scope){
	    	scope.months = [
	    		"January",
	    		"February",
	    		"March",
	    		"April",
	    		"May",
	    		"June",
	    		"July",
	    		"August",
	    		"September",
	    		"October",
	    		"November",
	    		"December"
	    	];
	    }
	};
}]);