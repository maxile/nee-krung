angular.module('neekrungApp')
.directive('selectYear', ["URL_CONSTANT",function(URL_CONSTANT) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"model":"="
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/selectYear.html",
	    link : function(scope){
	    	var d = new Date();
	    	var year = d.getFullYear();
	    	scope.years = [];
	    	for(i=-5;i<=5;i++){
	    		scope.years.push(year+i);
	    	}
	    }
	};
}]);