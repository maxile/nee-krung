angular.module('neekrungApp')
.directive('neekrungPdf', ["URL_CONSTANT","uploadAPI","Notice",function(URL_CONSTANT,uploadAPI,Notice) {
  	return {
	    restrict: 'E',
	    scope: {
	    	"ngModel":"=",
	    },
	    templateUrl: URL_CONSTANT.HOST_NAME+"/template/form/choosePdf.html",
	    link:function(scope,element, attributes){

	    	scope.selected = false;
	    	scope.uploadding = false;

	    	var pdfForm = null;

	    	scope.cancle = function(){
	    		scope.uploadding = false;
			    scope.selected = false;
                pdfForm.value="";
	    	}


	    	scope.upload = function(){
	    		var fd = new FormData();
	    		
			    fd.append("neekrungPDF", pdfForm.files[0]);
			    scope.uploadding = true;

			    uploadAPI.pdf(fd)
			    .success(function(resp){
			    	scope.message = [resp.message];
			    	Notice.notice(scope);

			    	scope.uploadding = false;
			    	scope.selected = false;
                	scope.ngModel = resp.data;
                	
			    }).error(function(resp){
			    	scope.message = resp.message["neekrungPDF"];
			    	Notice.alert(scope);

			    	scope.uploadding = false;
			    	scope.selected = false;
			    });
	    	}

	    	element.bind("change", function (changeEvent) {
            	pdfForm = changeEvent.target;
            	if(pdfForm.files[0].size){
            		scope.$apply(function () {
                    	scope.selected = true;
                    });
            	}
        //     	if(pdfForm.files[0].size >= 10000000){
        //     		scope.message = ["This file is very fat,Please resize your file."];
        //     		pdfForm.value="";
			    	// Notice.alert(scope);
			    	// scope.selected = false;
        //     	}else{
        //     		scope.selected = true;
        //     	}
            });
	    }
	};
}]);