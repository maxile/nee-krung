<?php
/* This section can be removed if you would like to reuse the PHP example outside of this PHP sample application */

?>
<!doctype html>
    <head>
        <title>FlexPaper AdaptiveUI PHP Example</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="initial-scale=1,user-scalable=no,maximum-scale=1,width=device-width" />
        <style type="text/css" media="screen">
			html, body	{ height:100%; }
			body { margin:0; padding:0; overflow:auto; }
			#flashContent { display:none; }
        </style>

		<link rel="stylesheet" type="text/css" href="{{asset('flexpaper/css/flexpaper.css')}}" />

		<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>    

    	<script src="{{asset('flexpaper/js/jquery.extensions.min.js')}}" type="text/javascript"></script>    

    	<!--[if gte IE 10 | !IE ]><!-->
    
    
    
    	<script type="text/javascript" src="{{asset('flexpaper/js/three.min.js')}}"></script>
    	<!--<![endif]-->
    	<script type="text/javascript" src="{{asset('flexpaper/js/flexpaper.js')}}"></script>
    	<script type="text/javascript" src="{{asset('flexpaper/js/flexpaper_handlers.js')}}"></script>

    </head>
    <body>
			<div id="documentViewer" class="flexpaper_viewer" style="position:absolute;width:100%;height:100%"></div>
	        <?php
	        if(isset($doc)){
	            $doc = substr($doc,0,strlen($doc)-4);
	        }else{
	            $doc = "Report";
	        }

			$pdfFilePath = $configManager->getConfig('path.pdf') . $subfolder;
			?>
	        <script type="text/javascript">
		        function getDocumentUrl(document){
		        	var numPages 			= <?php echo getTotalPages($pdfFilePath . $doc . ".pdf") ?>;
					var url = "{/flex/service?doc={doc}&format={format}&subfolder=<?php echo $subfolder ?>&page=[*,0],{numPages}}";
						url = url.replace("{doc}",document);
						url = url.replace("{numPages}",numPages);
						return url;
		        }

				var config = { config : {

						 DOC : escape(getDocumentUrl("<?php echo $doc ?>")),
						 Scale : 0.6,
						 ZoomTransition : 'easeOut',
						 ZoomTime : 0.5,
						 ZoomInterval : 0.1,
						 FitPageOnLoad : true,
						 FitWidthOnLoad : false,
						 FullScreenAsMaxWindow : false,
						 ProgressiveLoading : false,
						 MinZoomSize : 0.2,
						 MaxZoomSize : 5,
						 SearchMatchAll : false,
						 InitViewMode : '',
						 EnableWebGL : true,
						 MixedMode : true,
						 RenderingOrder : 'html5,html5',

						 ViewModeToolsVisible : true,
						 ZoomToolsVisible : true,
						 NavToolsVisible : true,
						 CursorToolsVisible : true,
						 SearchToolsVisible : true,
  						 key : '',

						 JSONDataType : 'jsonp',

						 WMode : 'transparent',
  						 localeChain: 'en_US'
						 }};

				$('#documentViewer').FlexPaperViewer(config);


	        </script>
   </body>
</html>