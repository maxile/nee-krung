@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--earning graph start-->
            <section class="panel magazine-form" ng-controller="newsController as ctrl" ng-init="ctrl.initState('{{$id}}')" >
                <!-- <header class="panel-heading">
                    Magazines
                </header> -->
                <div class="panel-body" ng-init="ctrl.setReturnLink('{{route('news.list')}}')">
                    <div class="admin-header"> 
                        <h2>Activities</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('news.list')}}')"> Add new</button>
                    </div>
                    <hr>
                    <div>
                        <form role="form" ng-submit="ctrl.submitPost()">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label> Title </label>
                                    <input type="text" class="form-control" placeholder="Title" ng-model="ctrl.newPost.title">    
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Cover</label>
                                <div class="file-box">
                                    <neekrung-choose-image ng-model="ctrl.newPost.image.image_url" helper="'Must be 500x350 px'"></neekrung-choose-image>
                                    <div class="row" style="margin-bottom: 20px">
                                        <div class="col-sm-4">
                                            <label>Longtitude</label>
                                            <input class="form-control" placeholder="Longtitude" type="text" ng-model="ctrl.newPost.image.longitude"></input>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Latitude</label>
                                            <input class="form-control" placeholder="Latitude" type="text" ng-model="ctrl.newPost.image.latitude"></input>
                                        </div>
                                        
                                    </div> 
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-10">
                                    <label> Summary </label>
                                    <input type="text" class="form-control" placeholder="Summary" ng-model="ctrl.newPost.short_detail">    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label> 
                                        Contents
                                    </label>
                                    <neekrung-contents-form ng-model="ctrl.newPost.contents"></neekrung-contents-form>
                                    
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="ctrl.newPost.connect_payment" ng-true-value="true" ng-false-value="false"> Connect with payment module.
                                </label>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="ctrl.newPost.published" ng-true-value="true" ng-false-value="false"> Publish this post.
                                </label>
                            </div>

                            <div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="button" class="btn btn-default"> Preview</button>
                            </div>
                            
                        </form>
                        
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@stop