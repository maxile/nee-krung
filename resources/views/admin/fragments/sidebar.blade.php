<section class="sidebar" ng-controller="sidebarController as ctrl">
    <div class="user-panel">
        <div class="pull-left image">
            <img src="{{asset('image/user.jpg')}}" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <div class="std thin">Hello, </div>
            <div class="std thin">Mr. Maxile</div>
        </div>
    </div>
    <ul class="sidebar-menu">
        <!-- <li class="active">
            <a><span></span></a>
        </li> -->
        <li ng-class="ctrl.Global.selectedMenu=='magazine'?'active':''"><a href="{{route('magazine.list')}}"><span>Magazine</span></a></li>
        <li ng-class="ctrl.Global.selectedMenu=='recommended'?'active':''"><a href="{{route('recommended.list')}}"><span>Recommended</span></a></li>
        <li ng-class="ctrl.Global.selectedMenu=='wlm'?'active':''"><a href="{{route('wlm.list')}}"><span>World Longest Magazine</span></a></li>
        <li ng-class="ctrl.Global.selectedMenu=='citydetox'?'active':''"><a href="{{route('citydetox.list')}}"><span>City Detox</span></a></li>
        <li ng-class="ctrl.Global.selectedMenu=='news'?'active':''"><a href="{{route('news.list')}}"><span>Activities</span></a></li>
        <li ng-class="ctrl.Global.selectedMenu=='member'?'active':''"><a href="{{route('member.list')}}"><span>Member</span></a></li>
        <li ng-controller="authenController as ctrl" ng-click="ctrl.logout()" class="link">
            <a><span>Logout</span></a>
        </li>

    </ul>
</section>