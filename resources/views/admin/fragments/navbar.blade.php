<a class="logo">
    <img src="{{asset('image/logo.png')}}" style="height: 100%;padding: 5px 0px;">
    
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
                <a href="#" >
                    <i class="fa fa-user"></i>
                    <span>Maxile co.,ltd</span>
                </a>
                <!-- <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="fa fa-user"></i>
                    <span>Pakawat Nakwijit <i class="caret"></i></span>
                </a> -->
                <!-- <ul class="dropdown-menu dropdown-custom dropdown-menu-right">
                    <li class="dropdown-header text-center">Account</li>

                    <li>
                        <a href="#">
                            Profile
                        </a>
                        <a data-toggle="modal" href="#modal-user-settings">
                            Settings
                        </a>
                    </li>

                    <li class="divider"></li>

                    <li ng-controller="authenController as ctrl" ng-click="ctrl.logout()">
                        <a class="link"> Logout</a>
                    </li>
                </ul> -->
            </li>
        </ul>
    </div>
</nav>