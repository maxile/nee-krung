@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--earning graph start-->
            <section class="panel magazine-form" ng-controller="memberController as ctrl" ng-init="ctrl.initState('{{$id}}')" >
                <!-- <header class="panel-heading">
                    Magazines
                </header> -->
                <div class="panel-body" ng-init="ctrl.setReturnLink('{{route('member.list')}}')">
                    <div class="admin-header"> 
                        <h2>Member</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('member.list')}}')"> Add new</button>
                    </div>
                    <hr>
                    <div>
                        <form role="form" ng-submit="ctrl.submitMember()">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label> E-mail </label>
                                    <input type="email" class="form-control" placeholder="E-mail" ng-model="ctrl.newMember.email" ng-disabled="ctrl.editVersion">    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label> 
                                        Password 
                                        <span style="font-weight: 200;" ng-show="ctrl.editVersion"> ** fill this box if you want to change your password</span>
                                    </label>

                                    <input type="password" class="form-control" placeholder="Password" ng-model="ctrl.newMember.password">    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label> 
                                        Password Again
                                        <span style="font-weight: 200;" ng-show="ctrl.editVersion"> ** fill this box if you want to change your password</span>
                                    </label>
                                    <input type="password" class="form-control" placeholder="Password" ng-model="ctrl.newMember.repassword" match="ctrl.newMember.password">    
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Profile</label>
                                <div class="file-box">
                                    <neekrung-choose-image ng-model="ctrl.newMember.image_url" helper="'Must be 1800x600 px'"></neekrung-choose-image>
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label> Name(TH) </label>
                                    <input type="text" class="form-control" placeholder="Name" ng-model="ctrl.newMember.name_th">    
                                </div>
                                <div class="col-sm-6">
                                    <label> Surname(TH) </label>
                                    <input type="text" class="form-control" placeholder="Surname" ng-model="ctrl.newMember.surname_th">    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-6">
                                    <label> Name(ENG) </label>
                                    <input type="text" class="form-control" placeholder="Name" ng-model="ctrl.newMember.name_eng">    
                                </div>
                                <div class="col-sm-6">
                                    <label> Surname(ENG) </label>
                                    <input type="text" class="form-control" placeholder="Surname" ng-model="ctrl.newMember.surname_eng">    
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label> Education Field </label>
                                    <input type="text" class="form-control" placeholder="ex. Marketing" ng-model="ctrl.newMember.education_field">    
                                </div>
                                <div class="col-sm-4">
                                    <label> University </label>
                                    <input type="text" class="form-control" placeholder="ex. XXX University" ng-model="ctrl.newMember.university">    
                                </div>

                                <div class="col-sm-4">
                                    <label> University Address </label>
                                    <input type="text" class="form-control" placeholder="ex. Bangkok, Thailand" ng-model="ctrl.newMember.university_address">    
                                </div>
                            </div>

                            <!-- <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="ctrl.newPost.published" ng-true-value="true" ng-false-value="false"> Publish this post.
                                </label>
                            </div> -->

                            <div>
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                            
                        </form>
                        
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@stop