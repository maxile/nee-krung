@extends('admin.app')

@section('content')
	<div class="row">
		<div class="col-md-12">
            <!--earning graph start-->
            <section class="panel" ng-controller="recommendedController as ctrl">
                <div class="panel-body" ng-init="ctrl.fetchPost()">
                    <div class="admin-header"> 
                        <h2>Recommended</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('recommended.form')}}')"> Add new</button>
                    </div>
                    <div>
                        
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Title</th>
                                <th>Last Updated</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            <tr ng-repeat="post in ctrl.posts">
                                <td> @{{$index+1}}</td>
                                <td> @{{post.title}}</td>
                                <td> @{{post.updated_at}} </td>
                                <td>
                                    <span class="badge bg-green" ng-if="post.published"> Publish</span>
                                    <span class="badge bg-red" ng-if="!post.published"> Not Active</span>
                                </td>
                                <td> 
                                    <button class="btn btn-default" ng-click="ctrl.linkTo('{{route('recommended.list')}}'+'/'+post._id+'/edit')"> Edit</button>
                                    <button class="btn btn-default" ng-click="ctrl.deletePost($index)"> Delete</button>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                    
                </div>
			</section>
		</div>
    </div>
@stop