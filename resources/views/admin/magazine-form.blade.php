@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--earning graph start-->
            <section class="panel magazine-form" ng-controller="magazineController as ctrl" ng-init="ctrl.initState('{{$id}}')" >
                <!-- <header class="panel-heading">
                    Magazines
                </header> -->
                <div class="panel-body" ng-init="ctrl.setReturnLink('{{route('magazine.list')}}')">
                    <div class="admin-header"> 
                        <h2>Magazines</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('magazine.list')}}')"> Add new</button>
                    </div>
                    <hr>
                    <div>
                        <form role="form" ng-submit="ctrl.submitMagazine()">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label>Vol.</label>
                                    <input type="text" class="form-control" placeholder="Vol." ng-model="ctrl.newMagazine.vol">    
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label>Month</label>
                                    <select-month model="ctrl.newMagazine.month"></select-month>
                                </div>
                                <div class="col-sm-4">
                                    <label>Year</label>
                                    <select-year model="ctrl.newMagazine.year"></select-year>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Magazine Cover</label>
                                <div class="file-box">
                                    <neekrung-choose-image ng-model="ctrl.newMagazine.cover" helper="'Must be 135x180 px'"></neekrung-choose-image>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Banner</label>
                                <div class="file-box">
                                    <neekrung-choose-image ng-model="ctrl.newMagazine.image.image_url" helper="'Must be 1280x600 px'"></neekrung-choose-image>
                                    <div style="margin-bottom: 20px">
                                        <label>Caption</label>
                                        <input class="form-control" placeholder="Caption" type="text" ng-model="ctrl.newMagazine.image.caption"></input>
                                    </div> 
                                    <div class="row" style="margin-bottom: 20px">
                                        <div class="col-sm-4">
                                            <label>Longtitude</label>
                                            <input class="form-control" placeholder="Longtitude" type="text" ng-model="ctrl.newMagazine.image.longitude"></input>
                                        </div>
                                        <div class="col-sm-4">
                                            <label>Latitude</label>
                                            <input class="form-control" placeholder="Latitude" type="text" ng-model="ctrl.newMagazine.image.latitude"></input>
                                        </div>
                                        
                                    </div> 
                                </div>
                            </div>

                            <div class="form-group">
                                <label>PDF File</label>
                                <div class="file-box">
                                    <neekrung-pdf ng-model="ctrl.newMagazine.magazine_file"></neekrung-pdf>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="ctrl.newMagazine.published" ng-true-value="true" ng-false-value="false"> Publish this magazine.
                                </label>
                            </div>

                            <button type="submit" ng-disabled="!ctrl.canSubmit" class="btn btn-info">Submit</button>
                        </form>
                        
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@stop