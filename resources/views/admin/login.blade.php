@extends('admin.master')

@section('outter')
	<section class="container login-form" ng-controller="authenController as ctrl">
		<article>
			<form role="form" ng-submit="ctrl.login()">
	            <div class="form-group">
	                <label>E-mail</label>
	                <input type="email" class="form-control" placeholder="E-mail" ng-model="ctrl.member.email">    
	            </div>

	            <div class="form-group">
	                <label>Password</label>
	                <input type="password" class="form-control" placeholder="E-mail" ng-model="ctrl.member.password">    
	            </div>

	            <div class="form-group">
	                <button class="btn btn-default"> Login</button>
	            </div>
			</form>	
		</article>
		
	</section>
@stop