@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--earning graph start-->
            <section class="panel magazine-form" ng-controller="wlmController as ctrl" ng-init="ctrl.initState('{{$id}}')" >
                <!-- <header class="panel-heading">
                    Magazines
                </header> -->
                <div class="panel-body" ng-init="ctrl.setReturnLink('{{route('wlm.list')}}')">
                    <div class="admin-header"> 
                        <h2>World Longest Magazine</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('wlm.list')}}')"> Add new</button>
                    </div>
                    <hr>
                    <div>
                        <form role="form" ng-submit="ctrl.submitPost()">
                            <div class="form-group row">
                                <div class="col-sm-4">
                                    <label> Title </label>
                                    <input type="text" class="form-control" placeholder="Title" ng-model="ctrl.newPost.title">    
                                </div>
                            </div>

                            <div class="form-group">
                                <label>Cover</label>
                                <div class="file-box">
                                    <neekrung-choose-image ng-model="ctrl.newPost.image.image_url" helper="''"></neekrung-choose-image>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-sm-12">
                                    <label> 
                                        Contents
                                    </label>
                                    <neekrung-contents-form ng-model="ctrl.newPost.contents" template-list="['text','img']"></neekrung-contents-form>
                                    
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" ng-model="ctrl.newPost.published" ng-true-value="true" ng-false-value="false"> Publish this post.
                                </label>
                            </div>
                            <div>
                                <button type="submit" class="btn btn-info">Submit</button>
                                <button type="button" class="btn btn-default"> Preview</button>
                            </div>
                            
                        </form>
                        
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@stop