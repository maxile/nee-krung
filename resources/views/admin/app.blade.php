@extends('admin.master')

@section('navbar')
    <header class="header">
        @include('admin.fragments.navbar')
    </header>
@stop

@section("sidebar")
    <aside class="left-side sidebar-offcanvas">
        @include('admin.fragments.sidebar')
    </aside>
@stop