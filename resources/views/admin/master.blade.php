<!DOCTYPE html>
<html lang="en" ng-app="neekrungApp">
<head>
    <meta charset="UTF-8">
    <title>Neekrung Magazine | Dashboard</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta name="description" content="Developed By M Abdur Rokib Promy">
    <meta name="keywords" content="Admin, Bootstrap 3, Template, Theme, Responsive">
    <link rel="shortcut icon" type="image/png" href="{{asset('image/favicon.png')}}"/>
    <!-- font Awesome -->
    <!-- <link href="css/font-awesome.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- Ionicons -->
    <!-- <link href="css/ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <!-- <link href="css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" /> -->

    <!-- <link href='https://fonts.googleapis.com/css?family=Kanit:200,500&subset=thai,latin' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'> -->
    <!-- Theme style -->
    <link href="{{asset('css/admin_style.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('css/all-admin.css')}}" rel="stylesheet" type="text/css" />

    <title>Neekrung Magazine Admin</title>

    <link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />    
    <link href="{{asset('bower_components/css-spinners/css/spinners.css')}}" rel="stylesheet" type="text/css" />    
    <link rel='stylesheet' href="{{asset('bower_components/textAngular/dist/textAngular.css')}}" />
    

    <script src="{{asset('bower_components/angular/angular.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('js/all-admin.min.js')}}" type="text/javascript"></script>   
    <script type="text/javascript">
        angular.module('neekrungApp')
        .constant('URL_CONSTANT',{
            "HOST_NAME": "{{env('HOST_NAME','/')}}",
            "ROOT_PATH" : "{{env('ADMIN_ROOT_PATH','')}}",
            "API_PATH" : "{{env('APP_PATH','/')}}",
        });

    </script>  
</head>
<body class="skin-black">
    @section('navbar')
    @show

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- Left side column. contains the logo and sidebar -->
        @section("sidebar")
        @show
        
        @section('outter')
        @show
        <aside class="right-side">
            <!-- Main content -->
            <section class="content">
                @section('content')
                @show
            </section><!-- /.content -->
        </aside><!-- /.right-side -->

    </div><!-- ./wrapper -->

    <script src="{{asset('bower_components/angular-route/angular-route.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('bower_components/angular-animate/angular-animate.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('bower_components/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>

    <script src="{{asset('bower_components/textAngular/dist/textAngular-rangy.min.js')}}"></script>
    <script src="{{asset('bower_components/textAngular/dist/textAngular-sanitize.min.js')}}"></script>
    <script src="{{asset('bower_components/textAngular/dist/textAngular.min.js')}}"></script>
    
    <script src="{{asset('js/admin_app.js')}}" type="text/javascript"></script>


</body>
</html>