@extends('admin.app')

@section('content')
	<div class="row">
		<div class="col-md-12">
            <!--earning graph start-->
            <section class="panel" ng-controller="magazineController as ctrl">
                <!-- <header class="panel-heading">
                	Magazines
                </header> -->
                <div class="panel-body" ng-init="ctrl.fetchMagazines()">
                    <div class="admin-header"> 
                        <h2>Magazines</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('magazine.form')}}')"> Add new</button>
                    </div>
                    <div>
                        
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>Vol.</th>
                                <th>Date</th>
                                <th>Status</th>
                                <th></th>
                            </tr>
                            <tr ng-repeat="magazine in ctrl.magazines">
                                <td> @{{$index+1}}</td>
                                <td>Vol. @{{magazine.vol}}</td>
                                <td> @{{magazine.month | month}} @{{magazine.year}} </td>
                                <td>
                                    <span class="badge bg-green" ng-if="magazine.published"> Publish</span>
                                    <span class="badge bg-red" ng-if="!magazine.published"> Not Active</span>
                                </td>
                                <td> 
                                    <button class="btn btn-default" ng-click="ctrl.linkTo('{{route('magazine.list')}}'+'/'+magazine._id+'/edit')"> Edit</button>
                                    <button class="btn btn-default" ng-click="ctrl.deleteMagazine($index)"> Delete</button>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                    
                </div>
			</section>
		</div>
    </div>
@stop