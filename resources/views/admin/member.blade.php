@extends('admin.app')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <!--earning graph start-->
            <section class="panel" ng-controller="memberController as ctrl">
                <div class="panel-body" ng-init="ctrl.fetchMember()">
                    <div class="admin-header"> 
                        <h2>Member</h2> 
                        <button class="btn btn-neekrung" ng-click="ctrl.linkTo('{{route('member.form')}}')"> Add new</button>
                    </div>
                    <div>
                        
                        <table class="table table-striped">
                            <tr>
                                <th style="width: 10px">#</th>
                                <th>E-mail</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th></th>
                            </tr>
                            <tr ng-repeat="member in ctrl.members">
                                <td> @{{$index+1}}</td>
                                <td> @{{member.email}}</td>
                                <td> @{{member.name_eng}} </td>
                                <td> @{{member.surname_eng}} </td>
                                <td> 
                                    <button class="btn btn-default" ng-click="ctrl.linkTo('{{route('member.list')}}'+'/'+member._id+'/edit')"> Edit</button>
                                    <button class="btn btn-default" ng-click="ctrl.deleteMember($index)"> Delete</button>
                                </td>
                            </tr>
                            
                        </table>
                    </div>
                    
                </div>
            </section>
        </div>
    </div>
@stop