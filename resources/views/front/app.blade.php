<!DOCTYPE html>
<html lang="en" ng-app="neekrungApp">
<head>
	<base href="/">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" type="image/png" href="{{asset('image/favicon.png')}}"/>

	<title>Neekrung Magazine</title>

	<link href='https://fonts.googleapis.com/css?family=Kanit:200,500&subset=thai,latin' rel='stylesheet' type='text/css'>
	<link href="{{asset('bower_components/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />    
	<link href="{{asset('bower_components/css-spinners/css/spinners.css')}}" rel="stylesheet" type="text/css" />    
	<link href="{{asset('bower_components/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css" />    
	<link href="{{asset('css/hover-effect.css')}}" rel="stylesheet" type="text/css" />    
	<link rel='stylesheet' href="{{asset('bower_components/textAngular/dist/textAngular.css')}}" />
	<link href="{{asset('css/all.css')}}" rel="stylesheet" type="text/css" />    
	<link rel='stylesheet' href="{{asset('bower_components/textAngular/dist/textAngular.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('flexpaper/css/flexpaper.css')}}" />
	<link rel="stylesheet" type="text/css" href="{{asset('css/magazine.css')}}" />
	
	<script src="{{asset('bower_components/angular/angular.min.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/angular-route/angular-route.min.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap.min.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/angular-animate/angular-animate.min.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/angular-facebook/lib/angular-facebook.js')}}" type="text/javascript"></script>    
	<script src="{{asset('bower_components/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>    

	<script src="{{asset('bower_components/textAngular/dist/textAngular-rangy.min.js')}}"></script>
    <script src="{{asset('bower_components/textAngular/dist/textAngular-sanitize.min.js')}}"></script>
    <script src="{{asset('bower_components/textAngular/dist/textAngular.min.js')}}"></script>
    
    <!-- <script src="{{asset('bower_components/lodash/dist/lodash.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-simple-logger/dist/angular-simple-logger.min.js')}}"></script>
    <script src="{{asset('bower_components/angular-google-maps/dist/angular-google-maps.min.js')}}"></script> -->
    

    <script src="{{asset('flexpaper/js/jquery.extensions.min.js')}}" type="text/javascript"></script>    
    <script src="{{asset('/js/turn.min.js')}}" type="text/javascript"></script>    
    
    <!--[if gte IE 10 | !IE ]><!-->
    
    
    
    <script type="text/javascript" src="{{asset('flexpaper/js/three.min.js')}}"></script>
    <!--<![endif]-->
    <script type="text/javascript" src="{{asset('flexpaper/js/flexpaper.js')}}"></script>
    <script type="text/javascript" src="{{asset('flexpaper/js/flexpaper_handlers.js')}}"></script>

	<script src="{{asset('js/hover-effect.js')}}" type="text/javascript"></script>    

	<script src="{{asset('js/all.min.js')}}" type="text/javascript"></script>    
	<script type="text/javascript">
		angular.module('neekrungApp')
		.constant('URL_CONSTANT',{
		    "HOST_NAME": "{{env('HOST_NAME','/')}}",
		    "ROOT_PATH" : "{{env('ROOT_PATH','')}}",
		    "API_PATH" : "{{env('APP_PATH','/')}}",
		});

	</script>
</head>
<body ng-controller="mainController as mainCtrl" style="font-family: Kanit;">
	<div id="top"></div>
	<neekrung-navbar></neekrung-navbar>
	<div ng-view ng-show="mainCtrl.global.initialComplete"></div>
	<neekrung-footer sponsor="mainCtrl.sponsor" ng-show="mainCtrl.global.initialComplete"></neekrung-footer>
</body>
</html>
