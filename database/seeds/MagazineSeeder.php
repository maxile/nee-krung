<?php namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Models\Magazine;

class MagazineSeeder extends Seeder {

	public function run()
	{
		Model::unguard();
		DB::table('magazines')->delete();

		$magazines = [
			[
				"image"=> [
					"image_url"=> "image/book/1.jpg",
					"longitude"=>0,
					"latitude"=>0
				],
				"vol"=>2,
				"month"=>9,
				"year"=>2013,
				"published"=>false,
				"magazine_file"=>"demo1",
				"banners"=>[
					[ "image_url"=>"image/banner/slide1.jpg","caption"=>"ภาพ...มีระยะ ชีวิต...ก็เป็นแบบนั้น"],
				]
			],
			[
				"image"=> [
					"image_url"=> "image/book/2.jpg",
					"longitude"=>0,
					"latitude"=>0
				],
				"vol"=>3,
				"month"=>10,
				"year"=>2013,
				"published"=>false,
				"magazine_file"=>"demo2",
				"banners"=>[
					[ "image_url"=>"image/banner/slide2.jpg","caption"=>"สีสัน ทะเลไทย"],
				]
			],
			[
				"image"=> [
					"image_url"=> "image/book/4.jpg",
					"longitude"=>0,
					"latitude"=>0
				],
				"vol"=>4,
				"month"=>11,
				"year"=>2013,
				"published"=>true,
				"magazine_file"=>"demo3",
				"banners"=>[
					[ "image_url"=>"image/banner/slide3.jpg","caption"=>"เหนื่อยนัก.. ก็พักบ้าง"],
				]
			],
		];

		foreach ($magazines as $magazine) {
			$m = new Magazine;
			$m->image = $magazine["image"];
			$m->vol = $magazine["vol"];
			$m->month = $magazine["month"];
			$m->year = $magazine["year"];
			$m->published = $magazine["published"];
			$m->banners = $magazine["banners"];
			$m->magazine_file = $magazine["magazine_file"];
			$m->link="";
			$m->save();
		}

		
	}
}
?>