<?php namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Models\Section;

class SectionSeeder extends Seeder {

	public function run()
	{
		Model::unguard();
		DB::table('sections')->delete();

		$sections = [
			
			[
				"name"=>"recommended",
				"title"=>"Recommended",
				"template"=> "preview-3-col.html"
			],
			[
				"name"=>"city-detox",
				"title"=>"City detox",
				"template"=> "preview-img-right.html",
			],
			[
				"name"=>"wlm",
				"title"=>"World Longest Magazine",
				"template"=> "preview-3-col-extra.html",
			],
			[
				"name"=>"news",
				"title"=>"Activities",
				"template"=> "preview-img-left.html",
			]
		];

		foreach ($sections as $section) {
			$s = new Section;

			$s->name = $section["name"];
			if(array_key_exists("title", $section)){
				$s->title = $section["title"];
			}

			$s->template = $section["template"];
			$s->save();
		}
		
	}
}
?>