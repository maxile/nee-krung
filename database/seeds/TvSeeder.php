<?php namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Models\TV;
use App\Models\Playlist;

class TvSeeder extends Seeder {

	public function run()
	{
		Model::unguard();
		DB::table('Playlist')->delete();
		DB::table('TVs')->delete();

		$TVs = [
			"highlight"=>"YQHsXMglC9A",
			"playlist"=>[
				[
					"title" =>"หนีกรุง Cinema",
					"video" => [ "mtf7hC17IBM","hq2KgzKETBw","zETVr04XUE4"]
				],
				[
					"title" =>"หนีกรุง Special",
					"video" => [ "MwpMEbgC7DA","hq2KgzKETBw","FrLequ6dUdM"]
				],

			]
		];

		$tv = new TV;
		$tv->highlight = $TVs["highlight"];
		foreach ($TVs["playlist"] as $playlist) {
			$p = new Playlist;
			$p->title = $playlist["title"];
			$p->videos = $playlist["video"];
			$p->save();
		}	

		
	}
}
?>