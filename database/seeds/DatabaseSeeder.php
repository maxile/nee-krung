<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('\App\Seeder\MagazineSeeder');
		$this->call('\App\Seeder\SectionSeeder');
		$this->call('\App\Seeder\UserSeeder');
		$this->call('\App\Seeder\MemberSeeder');
		// $this->call('PostSeeder');
		$this->call('\App\Seeder\TvSeeder');

	}

}
