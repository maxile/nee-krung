<?php namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Hash;

use App\Models\User;

class UserSeeder extends Seeder {

	public function run()
	{
		Model::unguard();
		DB::table('users')->delete();

		$u = new User;
		$u->image_url = "image/user.jpg";
		$u->email = "admin@neekrung.com";
		$u->password = Hash::make("neekrung");
		$u->name = "Neekrung";
		$u->surname = "Magazine";
		$u->promotion = ["name"=>"free"];
		$u->fb_access_token = NULL;
		$u->fb_id = NULL;
		$u->save();
		
	}
}
?>