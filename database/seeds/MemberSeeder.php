<?php namespace App\Seeder;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Hash;
use App\Models\Member;

class MemberSeeder extends Seeder {

	public function run()
	{
		Model::unguard();
		DB::table('members')->delete();

		$m = new Member;
		$m->image_url = "image/editor/1.png";
		$m->email = "admin@neekrung.com";
		$m->password = Hash::make("neekrung");

		$m->name_th = "หนีกรุง";
		$m->surname_th = "ไปปรุงฝัน";
		$m->name_eng = "Neekrung";
		$m->surname_eng = "Magazine";

		$m->education_field = "Marketing";
		$m->university = "Dhurakijpundit University";
		$m->university_address = "Bangkok, Thailand";
		$m->is_admin = true;
		$m->layout = 1;
		$m->save();
		
	}
}
?>